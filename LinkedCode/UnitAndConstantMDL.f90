MODULE UnitAndConstantMDL
!u_Angstrom:         1 Angstrom in atomic unit.           Source== https://physics.nist.gov/cgi-bin/cuu/Value?tbohrrada0|search_for=atomic+unit
!u_AttoSecond:       1 attosecond in atomic unit.         Source== https://physics.nist.gov/cgi-bin/cuu/Value?aut|search_for=atomic+unit
!u_eV:               1 electron volt in atomic unit.      Source== https://physics.nist.gov/cgi-bin/cuu/Value?evhr|search_for=hartree
!u_FemtoSecond:      1 femtosecond in atomic unit.        Source== the same as that in u_AttoSecond.
!u_LightSpeed:       speed of light in atomic unit.       Source== https://physics.nist.gov/cgi-bin/cuu/Value?alph|search_for=abbr_in!
!u_NanoMeter:        1 nanometer in atomic unit.          Source== the same as that in u_Angstrom
!u_VoltOverAngstrom: 1 Volt over Angstrom in atomic unit. Source== https://physics.nist.gov/cgi-bin/cuu/Value?auefld|search_for=atomic+unit
!usi_C:   1 Coulomb in atomic unit.Source== https://physics.nist.gov/cgi-bin/cuu/Value?e|search_for=elecmag_in!
!usi_J:	  1 Joul in atomic unit.   Source== https://physics.nist.gov/cgi-bin/cuu/Value?hr|search_for=energy
!usi_Kg:  1 Kg in atomic unit.     Source== https://physics.nist.gov/cgi-bin/cuu/Value?ttme|search_for=time
!usi_M:   1 meter in atomic unit.  Source== https://physics.nist.gov/cgi-bin/cuu/Value?tbohrrada0|search_for=length	
!usi_S:   1 second in atomic unit. Source== https://physics.nist.gov/cgi-bin/cuu/Value?aut|search_for=time
implicit none
!first level constants
integer, parameter :: dp=kind(0.d0)
real(kind=dp), parameter :: pi=dacos(-1.d0) 
real(kind=dp), parameter :: u_Angstrom=1.d0/0.52917721067d0
real(kind=dp), parameter :: u_AttoSecond=0.1d0/2.418884326509d0
real(kind=dp), parameter :: u_as = u_Attosecond
real(kind=dp), parameter :: u_eV=3.674932248d0/100.d0
real(kind=dp), parameter :: u_FemtoSecond=100.d0/2.418884326509d0
real(kind=dp), parameter :: u_fs = u_FemtoSecond
real(kind=dp), parameter :: u_LightSpeed=137.035999139d0
real(kind=dp), parameter :: u_NanoMeter=10.d0/0.52917721067d0
real(kind=dp), parameter :: u_nm = u_NanoMeter
real(kind=dp), parameter :: u_VoltOverAngstrom=1.d0/51.42206707d0
real(kind=dp), parameter :: usi_C=1.d19/1.6021766208d0
real(kind=dp), parameter :: usi_J=1.d18/4.359744650d0
real(kind=dp), parameter :: usi_Kg=1.d31/9.10938356d0
real(kind=dp), parameter :: usi_M=1.d10/0.52917721067d0
real(kind=dp), parameter :: usi_S=1.d17/2.418884326509d0
!second level constants
!third level constants
real(kind=dp), parameter :: charge_e=-1.d0
real(kind=dp), parameter :: hbar=1.d0
real(kind=dp), parameter :: mass_e=1.d0
real(kind=dp), parameter :: u_PermittivityVacuum=1.d0/4.d0/pi
real(kind=dp), parameter :: u_PermeabilityVacuum=1.d0/(u_PermittivityVacuum*u_LightSpeed*u_LightSpeed)
real(kind=dp), parameter :: u_TW_Over_CM2=1.d12*usi_J/usi_S/((usi_M*0.01d0)**2.d0)/(u_LightSpeed/8.d0/pi)
!old constants
real(kind=dp), parameter :: u_fs2au=41.34137334d0
real(kind=dp), parameter :: u_nm2au=18.89726125d0
real(kind=dp), parameter :: u_TW2au=2.849451309d0/100000.d0
real(kind=dp), parameter :: u_eV2au=0.03674932d0
real(kind=dp), parameter :: u_VoverA_2au=1.d0/51.4220826d0

public

CONTAINS


function ConversionIntensityToElectricField(direction,input,refractive_index)
!For direction==F (forward)
!Input is the vacuum intensity in TW/(cm**2), output is the corresponding electric field in au.
!For direction==B (backward)
!Input is the electric field in au, output is the corresponding intensity in TW/(cm**2)
implicit none
real(dp) :: ConversionIntensityToElectricField
character, intent(in) :: direction
real(dp), intent(in) :: input, refractive_index
!internal
real(kind=dp) :: TeraWattOverCm2=1.d12*usi_J/usi_S/((usi_M*0.01d0)**2.d0), COver8pi=u_LightSpeed/8.d0/pi
if(refractive_index.ne.1.d0) stop "M=LaserFieldMDL;S=ConversionIntensityToOthers;== refractive_index not equal to unity is not yet incorporated in this subroutine!"
if(direction=='F') then
  !remember intensity is defined as time-averaged poynting vector(assuming plane wave=> I=0.5*c*epsilon0*E0^2, where 0.5 comes from time average.)
  ConversionIntensityToElectricField=dsqrt(input*TeraWattOverCm2/COver8pi)
elseif(direction=='B') then
  ConversionIntensityToElectricField=(input**2.d0)*COver8pi/TeraWattOverCm2
else
  stop "M=LaserFieldMDL;S=ConversionIntensityToOthers;==invalid choose"
endif
endfunction ConversionIntensityToElectricField


function ConversionWavelengthToOthers(choose,lumbda,refractive_index)
!Given a vacuum wavelength in nm, calculate the desired output paprameter in au.
!choose: specifying the desired output
!        0: wavelength in au in a medium
!        1: radian frequency in au
!        2: period in au
implicit none
real(dp) :: ConversionWavelengthToOthers
integer, intent(in) :: choose
real(dp), intent(in) :: lumbda, refractive_index
!implicit
real(dp) :: lumbda0
lumbda0=lumbda*u_NanoMeter !lumbda in au
if(choose==0) then
  ConversionWavelengthToOthers=lumbda0/refractive_index
elseif(choose==1) then
  ConversionWavelengthToOthers=2.d0*pi*u_LightSpeed/lumbda0
elseif(choose==2) then
  ConversionWavelengthToOthers=lumbda0/u_LightSpeed
else
  stop "M=LaserFieldMDL;S=ConversionWavelengthToOthers;==invalid choose!"
endif
endfunction ConversionWavelengthToOthers


function InquiryFourierDiscrete1(n,periodicity,x,FourierComponent)
!Given Fourier components of a REAL periodic function f(x), return the value of f at a certain point.
!n: the highest mode of Fourier expansion. n>=1
!periodicity: the period of f in domain x.
!FourierComponent: the Fourier components of f
implicit none
integer :: n
real(dp) :: InquiryFourierDiscrete1, periodicity, x
complex(dp) :: FourierComponent(0:n)
integer :: i
real(kind=8) :: f
f=dble(FourierComponent(0))
do i=1,n
  f=f+2.d0*dble(FourierComponent(i)*cdexp(dcmplx(0.d0,dble(i)*2.d0/periodicity*x)))
enddo
InquiryFourierDiscrete1=f
endfunction InquiryFourierDiscrete1


subroutine Periodization(n,f,k_begin,k_end)
!This subroutine will transform the function in extended zone scheme into that of reduced zone scheme.
!One must make sure k_end-k_begin=period
!n: the number of point in function f
!f: the function in the extended zone scheme. It's a function of variable k.
!f_p: the function in the reduced zone scheme.
!k_begin: the beginning of varaible k.
!k_end: the end of the variable k.
implicit none
integer :: n
real(dp) :: f(n), f_p(n), k_begin, k_end
integer :: t
real(dp) :: g
g=k_end-k_begin
f_p=f-k_begin
do t=1, n
  f_p(t)=dmod(f_p(t),g)
  if(f_p(t)<0.d0) f_p(t)=f_p(t)+g
enddo
f=f_p+k_begin
endsubroutine


subroutine PerformFourierTranformation(nt,nw,frequency,signalT,signalW,time)
!real time signal
implicit none
integer :: nt, nw
real(dp) :: frequency(nw), signalT(nt), time(nt)
complex(dp) :: signalW(nw)
integer :: w, t
real(dp) :: dt, time_duration
complex(dp) :: summ
dt=time(2)-time(1)
time_duration=time(nt)-time(1)
do w=1,nw
  summ=dcmplx(0.d0,0.d0)
  do t=1,nt
    summ=summ+dcmplx(signalT(t),0.d0)*cdexp(dcmplx(0.d0,frequency(w)*time(t)))
  enddo
  signalW(w)=summ*dcmplx(dt/time_duration,0.d0)
enddo
endsubroutine


subroutine PerformFourierSeriesExpansion(nt,w_lower,w_upper,signalT,signalW,time)
!real time siganl
!one should make sure time(nt)-time(1)=one period, and thereby signalT(1)=signal(nt). 
implicit none
integer :: nt, w_lower, w_upper
real(dp) :: signalT(nt), time(nt)
complex(dp) :: signalW(w_lower:w_upper)
integer :: t, w
real(dp) :: dt, period, w0
complex(dp) :: summ
dt=time(2)-time(1)
period=time(nt)-time(1)
w0=2.d0*pi/period
if(w_upper<w_lower) stop "M=LaserFieldMDL;S=PerformFourierSeriesExpansion;==w_upper should be >= w _lower"
do w=w_lower,w_upper,1
  summ=dcmplx(0.d0,0.d0)
  do t=1,nt,1
    if((t==1).or.(t==nt)) then
      summ=summ+dcmplx(0.5d0*signalT(t),0.d0)*cdexp(dcmplx(0.d0,dble(w)*w0*time(t))) !first and last points have only half weight of the others
    else
      summ=summ+dcmplx(      signalT(t),0.d0)*cdexp(dcmplx(0.d0,dble(w)*w0*time(t)))
    endif
  enddo
  signalW(w)=summ*dcmplx(dt/period,0.d0)
enddo
endsubroutine 


ENDMODULE UnitAndConstantMDL



