MODULE LaserFieldMDL
use UnitAndConstantMDL
implicit none
type LaserParameters
  character(LEN=99) :: envelope
  character(LEN=99) :: kernel
  real(kind=8) :: amplitude
  real(kind=8) :: wavelength
endtype LaserParameters



contains

!===========
!Subroutines
!===========

subroutine LaserFieldGeneratorNew(n_color,n_time,ColorParameters,field,time)
!This is just a wrapper for the function LaserFieldGeneratorFunction so one can just call this to generate a pulse stored in an array called field.
!===Integer===
!n_color: number of color in the laser. The color is in the form of Envlp(t)*cos(wt-phi). The laser is the total sum of each colors.
!n_time: n_time +1 = number of time grid. 
!===Real Double===
!ColorParameters: see LaserFieldGeneratorFunction for details.
!field(0:n_time): field(i) is the laser field at ith time grid.
!time(0:n_time): time(i) is the time at ith time grid.
implicit none
integer :: n_color, n_time
real(kind=dp) :: ColorParameters(n_color,-100:100), field(0:n_time), time(0:n_time)
!dummy variables
integer :: t
field=0.d0
do t=0,n_time
  field(t)=LaserFieldGeneratorFunction(n_color,ColorParameters,time(t))
enddo
endsubroutine


subroutine LaserFieldGenerator(n_color,n_time,color_parameters,field,time)
!Everything is in atomic unit
!===Integer===
!n_color: number of color in the laser. The color is in the form of Envlp(t)*cos(wt-phi). The laser is the total sum of each colors.
!n_time: n_time +1 = number of time grid. 
!===Real Double===
!color_parameters(n_color,6): this contains the information of specific color.
!                             color_parameters(i,1): the field amplitude of ith color.          
!                             color_parameters(i,2): the wavelength of ith color. If wavelength>=1.d10, frequency is set to zero. This is for generating envelop field.
!                             color_parameters(i,3): the phase of ith color. Useful note: shift in time=period*phase/2pi. Phase is set to zero if wavelength>=1.d10. 
!                             color_parameters(i,4): the envelop for ith color. 1.d0:constant; 2.d0:cosine square; 3.d0:Gaussian.         
!                             color_parameters(i,5): the center of the envelop for ith color.
!                             color_parameters(i,6): the "width" of the envelop for ith color. It has different meaning for various envelop type. constant:not utilized; consin square:extension of cosine square; Gaussain:standard variation.
!field(0:n_time): field(i) is the laser field at ith time grid.
!time(0:n_time): time(i) is the time at ith time grid.
implicit none
integer :: n_color, n_time
real(kind=dp) :: color_parameters(n_color,6), field(0:n_time), time(0:n_time)
!temporary variables
integer :: i, t
real(kind=dp) :: E0, envlp(0:n_time), phi, w
!calculate the field
field=0.d0
envlp=0.d0
do i=1, n_color,1
  E0=color_parameters(i,1)
  w=2.d0*pi*u_LightSpeed/color_parameters(i,2);if(color_parameters(i,2)>=1.d10) w=0.d0
  phi=color_parameters(i,3);if(color_parameters(i,2)>=1.d10) phi=0.d0
  call calculate_envlp(int(color_parameters(i,4)),color_parameters(i,5),color_parameters(i,6))
  do t=0, n_time, 1
    field(t)=field(t)+envlp(t)*E0*dcos(w*time(t)-phi)
  enddo
enddo
!contained subroutines
contains
  subroutine calculate_envlp(envlp_type,center,width)
  implicit none
  integer :: envlp_type
  real(kind=dp) :: center, width
  integer :: t_p
  if(envlp_type==1) then
    do t_p=0, n_time, 1
      if(dabs(time(t_p)-center)<=0.5d0*width) then
        envlp(t_p)=1.d0
      else
        envlp(t_p)=0.d0
      endif
    enddo
  elseif(envlp_type==2) then
    do t_p=0, n_time, 1
      if(dabs(time(t_p)-center)<=0.5d0*width) then
        envlp(t_p)=dcos(pi*(time(t_p)-center)/width)*dcos(pi*(time(t_p)-center)/width)
      else
        envlp(t_p)=0.d0
      endif
    enddo
  elseif(envlp_type==3) then
    do t_p=0, n_time, 1
      envlp(t_p)=dexp(-(time(t_p)-center)*(time(t_p)-center)/2.d0/width/width)
    enddo
  else
    stop "Error!!!M=LaserFieldMDL;S=LaserFieldGenerator;SS=calculate_envlp;==invalid envlp_type"
  endif
  endsubroutine calculate_envlp
endsubroutine


subroutine WindowGenerator(n_time,WindowType,time,tp,window,wp)
!This is just a wrapper for the function WindowGeneratorFunction so one can just call this to generate a window stored in an array called window.
!See the description in WindowGeneratorFunction for actual definitions of variables.
implicit none
integer :: n_time, WindowType
real(kind=8) :: time(0:n_time), tp(50), window(0:n_time), wp(50)
integer :: t
do t=0,n_time
  window(t)=WindowGeneratorFunction(WindowType,time(t),tp,wp)
enddo
endsubroutine





!=========
!Functions
!=========


function LaserFieldGeneratorFunction(n_color,ColorParameters,PresentTime)
!This retrun the electric field at specified Time with given ColorParameters. The color is in the form of Envlp(t)*cos(w(t-t_begin)+phi). The laser is the total sum of each colors..
!Everything is in atomic unit
!===Integer===
!n_color: number of color in the laser.
!===Real Double===
!ColorParameters(n_color,-100:100): this contains the information of specific color. We shorten ColorParameters to cp below.
!  cp(i,0): the type of the laser of ith color. Remember that, although it's shown integer below, this input is double real.
!           0=constant(rectangular) laser field, namely DC field within a time window.
!           1=constant(rectangular)-envelop laser field.
!           2=cosine-square-envelop laser field.
!           3=Gaussian-envelop laser field.
!  cp(i,1): the intensity of ith color.          
!  cp(i,2): the wavelength of ith color.
!  cp(i,3): the phase of ith color. Useful note: shift in time=period*phase/2pi.
!  cp(i,4): t_begin, the beginning of the pulse. Note that you can also treat it as shifting time. Just keep in mind the laser is in the form mentioned in above.
!  cp(i,j): for negative j, the actual definitions change according to the type of the laser for ith color:
!           For cp(i,0)=0:
!             cp(i,-1): the beginning time of the constant(rectangular) envelop for ith color.
!             cp(i,-2): the end time of the constant(rectangular) envelop for ith color.
!           For cp(i,0)=1:
!             cp(i,-1): the beginning time of the constant(rectangular) envelop for ith color.
!             cp(i,-2): the end time of the constant(rectangular) envelop for ith color.
!           For cp(i,0)=2:
!             cp(i,-1): the center of the cosine square envelop for ith color.
!             cp(i,-2): the width of the cosine square envelop for ith color.
!           For cp(i,0)=3:
!             cp(i,-1): the center of the Gaussian envelop for ith color.
!             cp(i,-2): the standard deviation of Gaussian envelop for ith color.
!PresentTime: present time.
implicit none
integer :: n_color
real(kind=8) :: ColorParameters(n_color,-100:100), LaserFieldGeneratorFunction, PresentTime
!temporary variables
integer :: i
real(kind=dp) :: E0, field, phi, t_b, tp(50), w, wp(50)
!calculate the field
tp=0.d0;wp=0.d0;field=0.d0
do i=1, n_color,1
  E0=dsqrt(ColorParameters(i,1))
  w=2.d0*pi*u_LightSpeed/ColorParameters(i,2)
  phi=ColorParameters(i,3)
  t_b=ColorParameters(i,4)
  if(int(ColorParameters(i,0))==0) then
    !only cp(i,-2~1) are used
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(0,PresentTime,tp,wp)
  elseif(int(ColorParameters(i,0))==1) then
    !only cp(i,-2~4) are used
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(0,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==2) then
    !only cp(i,-2~4) are used 
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(2,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==3) then
    !only cp(i,1~4), cp(i,-1~-2) are used
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(3,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==5) then
    !only cp(i,-2~4) are used 
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(5,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==6) then ! sine ramp up, constant, and sine ramp down
    !only cp(i,-4~4) are used 
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    tp(3)=ColorParameters(i,-3)
    tp(4)=ColorParameters(i,-4)
    field=field+E0*WindowGeneratorFunction(1,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==7) then ! sine^2 ramp up, constant, and sine^2 ramp down
    !only cp(i,-4~4) are used 
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    tp(3)=ColorParameters(i,-3)
    tp(4)=ColorParameters(i,-4)
    field=field+E0*WindowGeneratorFunction(6,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  else
    stop "Error!!!M=LaserFieldMDL;F=LaserGeneratorFunction;==Invalid ColorParameter(0)!"
  endif
enddo
LaserFieldGeneratorFunction=field
endfunction LaserFieldGeneratorFunction


function WindowGeneratorFunction(WindowType,present_time,tp,wp)
!This function generates the value of a specified window(or envelop) at specific time present_time. The height of all window is one.
!WindowType: specifyin the type of window.
!present_time: present time.
!tp and wp: reserved data arrays for time and window parameters.
implicit none
integer :: WindowType
real(kind=8) :: present_time, WindowGeneratorFunction, tp(50), wp(50)
real(kind=8) :: r1, r2, r3
if(WindowType==0) then 
!constant window(rectangular window)
!tp1=begin of window
!tp2=end of window
  if((present_time>=tp(1)).and.(present_time<=tp(2))) then
    WindowGeneratorFunction=1.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==1) then
!sine ramp up, constant, sine ramp down window
!tp1=begin of sine ramp up
!tp2=end of sine ramp up=begin of constant window
!tp3=begin of sine ramp down=end of constant window
!tp4=end of sine ramp down
!elements in tp and wp not mentioned above are not referenced
  if(.not.( (tp(1)<tp(2)).and.(tp(2)<=tp(3)).and.(tp(3)<tp(4)) )) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=1!"
  r1=pi/(2.d0*(tp(2)-tp(1)))!radian frequency for sine of ramp up
  r2=pi/(2.d0*(tp(4)-tp(3)))!randian frequency for cosine of ramp down
  if((present_time>=tp(1)).and.(present_time<tp(2))) then
    WindowGeneratorFunction=dsin(r1*(present_time-tp(1)))
  elseif((present_time>=tp(2)).and.(present_time<tp(3))) then
    WindowGeneratorFunction=1.d0
  elseif((present_time>=tp(3)).and.(present_time<=tp(4))) then
    WindowGeneratorFunction=dcos(r2*(present_time-tp(3)))
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==2) then
!cosine square window
!tp1=center of the window
!wp1=width of the window
  r1=tp(1) !center of the cosine square
  r2=wp(1) !width of the window
  r3=pi/r2 !radian frequency of cosine square
  if(dabs(present_time-r1)<=0.5d0*r2) then
    WindowGeneratorFunction=dcos(r3*(present_time-r1))*dcos(r3*(present_time-r1))
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==3) then
!Gaussian window (not the normalized Gaussian, the value of Gaussian is one at the center of the Gaussian)
!tp1=center of the Gaussian
!wp1=standard deviation of Gaussian
  r1=tp(1) !center of Gaussian
  r2=wp(1) !standard deviation of Gaussian
  WindowGeneratorFunction=dexp(-(present_time-r1)*(present_time-r1)/2.d0/r2/r2)
elseif(WindowType==4) then
!polynomially decay window
!1-3x^2+2x^3  and 1 before beginning and zero after this decay
!tp1=beginning of the decay
!tp2=end of the decay
  if((tp(2)-tp(1))<=0) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=4!"
  r1=tp(2)-tp(1)
  if((present_time>=tp(1)).and.(present_time<=tp(2))) then
    WindowGeneratorFunction=1.d0-3.d0*((present_time-tp(1))/r1)**2.d0+2.d0*((present_time-tp(1))/r1)**3.d0
  elseif(present_time<tp(1)) then 
    WindowGeneratorFunction=1.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==5) then
!sine^4 window
!tp1=center of the window
!tp2=width of the window
  r1=tp(1) !center of the cosine^4
  r2=wp(1) !width of the window
  r3=pi/r2 !radian frequency of cosine square
  if(dabs(present_time-r1)<=0.5d0*r2) then
    WindowGeneratorFunction=dcos(r3*(present_time-r1))**4.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==6) then
!sine^2 ramp up, constant, sine^2 ramp down window
!tp1=begin of sine ramp up
!tp2=end of sine^2 ramp up=begin of constant window
!tp3=begin of sine^2 ramp down=end of constant window
!tp4=end of sine ramp down
!elements in tp and wp not mentioned above are not referenced
  if(.not.( (tp(1)<tp(2)).and.(tp(2)<=tp(3)).and.(tp(3)<tp(4)) )) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=6!"
  r1=pi/(2.d0*(tp(2)-tp(1)))
  r2=pi/(2.d0*(tp(4)-tp(3)))
  if((present_time>=tp(1)).and.(present_time<tp(2))) then
    WindowGeneratorFunction=dsin(r1*(present_time-tp(1)))**2
  elseif((present_time>=tp(2)).and.(present_time<tp(3))) then
    WindowGeneratorFunction=1.d0
  elseif((present_time>=tp(3)).and.(present_time<=tp(4))) then
    WindowGeneratorFunction=dcos(r2*(present_time-tp(3)))**2
  else
    WindowGeneratorFunction=0.d0
  endif
else
  stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid WindowType!"
endif
endfunction


ENDMODULE LaserFieldMDL



