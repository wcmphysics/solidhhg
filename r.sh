#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J SolidHHG
# Queue (Partition):
#SBATCH --partition=express
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
##SBATCH --ntasks-per-node=32
#
#SBATCH --mail-type=none
#SBATCH --mail-user=chang-ming.wang@mpsd.mpg.de
#
# Wall clock limit:
#SBATCH --time=0-00:15:00
#
#SBATCH -c 32 --threads-per-core=1
# Run the program:
srun ./a.e > log.log
