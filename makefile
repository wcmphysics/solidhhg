


#FC = mpiifort -O0 -xHOST -ipo -ip -qopenmp
FC = ifort -O3 -parallel -qopenmp
#FC = ifort -O0 -qopenmp -check bounds -traceback
LN = -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_sequential.a -Wl,--end-group -lpthread -lm
#LN = -llapack -lblas
IN = a
OUT = a
OBJ1 = UnitAndConstantMDL
OBJ2 = VisItWriterMDL
OBJ3 = InputOutputMDL
OBJ4 = parameter
OBJ5 = global
OBJ6 = LaserFieldMDL
#OBJ7 = MPI_MDL



#$(OUT).e : $(OBJ1).o $(OBJ2).o $(OBJ3).o $(OBJ4).o $(OBJ5).o $(OBJ6).o $(OBJ7).o $(IN).o
#	$(FC) -o $(OUT).e $(OBJ1).o $(OBJ2).o $(OBJ3).o $(OBJ4).o $(OBJ5).o $(OBJ6).o $(OBJ7).o $(IN).o $(LN)
$(OUT).e : $(OBJ1).o $(OBJ2).o $(OBJ3).o $(OBJ4).o $(OBJ5).o $(OBJ6).o $(IN).o
	$(FC) -o $(OUT).e $(OBJ1).o $(OBJ2).o $(OBJ3).o $(OBJ4).o $(OBJ5).o $(OBJ6).o $(IN).o $(LN)
$(IN).o : $(IN).f90
	$(FC) -c $(IN).f90 $(LN)
$(OBJ1).o : $(OBJ1).f90
	$(FC) -c $(OBJ1).f90 $(LN)
$(OBJ2).o : $(OBJ2).f90
	$(FC) -c $(OBJ2).f90 $(LN)
$(OBJ3).o : $(OBJ3).f90
	$(FC) -c $(OBJ3).f90 $(LN)
$(OBJ4).o : $(OBJ4).f90
	$(FC) -c $(OBJ4).f90 $(LN)
$(OBJ5).o : $(OBJ5).f90 $(OBJ6).o
	$(FC) -c $(OBJ5).f90 $(LN)
$(OBJ6).o : $(OBJ6).f90
	$(FC) -c $(OBJ6).f90 $(LN)
#$(OBJ7).o : $(OBJ7).f90
#	$(FC) -c $(OBJ7).f90 $(LN)
clean:
	rm *.o *.e *.mod *.dat *.vtk *.log tjob*
