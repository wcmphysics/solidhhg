
set grid
load "~/Gitlab/MyLinuxSetup/Gnuplot/palette/parula.pal"
set term png size 1920,1080 font "Helvetica,25" lw 2
set xlabel "Energy(eV)"
set ylabel "Log10(I) (au)"
set key top right

a=log10((182.0/142.0)**2)
b=log10((142.0/142.0)**2)
c=log10((102.0/142.0)**2)

set output "Conv0.87_S.png"
p "h1_40_142_500k.rec" i 0  w l ls 11 t 'E0=0.87V/nm; Ns=40, Nk=142, Nt=500k',\
  "h1_30_142_500k.rec" i 0  w l ls 12 t 'E0=0.87V/nm; Ns=30, Nk=142, Nt=500k',\
  "h1_20_142_500k.rec" i 0  w l ls 13 t 'E0=0.87V/nm; Ns=20, Nk=142, Nt=500k'

set output "Conv0.87_K.png"
p "h1_30_182_500k.rec" i 0 u 1:(a+$2) w l ls 11 t 'E0=0.87V/nm; Ns=30, Nk=182, Nt=500k',\
  "h1_30_142_500k.rec" i 0 u 1:(b+$2) w l ls 12 t 'E0=0.87V/nm; Ns=30, Nk=142, Nt=500k',\
  "h1_30_102_500k.rec" i 0 u 1:(c+$2) w l ls 13 t 'E0=0.87V/nm; Ns=30, Nk=102, Nt=500k'

set output "Conv0.87_T.png"
p "h1_30_142_1000k.rec" i 0  w l ls 11 t 'E0=0.87V/nm; Ns=30, Nk=142, Nt=1000k',\
  "h1_30_142_500k.rec"  i 0  w l ls 12 t 'E0=0.87V/nm; Ns=30, Nk=142, Nt=500k',\
  "h1_30_142_300k.rec"  i 0  w l ls 13 t 'E0=0.87V/nm; Ns=30, Nk=142, Nt=300k'



set output "Conv1.65_S.png"
p "h2_40_142_500k.rec" i 0  w l ls 11 t 'E0=1.65V/nm; Ns=40, Nk=142, Nt=500k',\
  "h2_30_142_500k.rec" i 0  w l ls 12 t 'E0=1.65V/nm; Ns=30, Nk=142, Nt=500k',\
  "h2_20_142_500k.rec" i 0  w l ls 13 t 'E0=1.65V/nm; Ns=20, Nk=142, Nt=500k'

set output "Conv1.65_K.png"
p "h2_30_182_500k.rec" i 0 u 1:(a+$2)  w l ls 11 t 'E0=1.65V/nm; Ns=30, Nk=182, Nt=500k',\
  "h2_30_142_500k.rec" i 0 u 1:(b+$2)  w l ls 12 t 'E0=1.65V/nm; Ns=30, Nk=142, Nt=500k',\
  "h2_30_102_500k.rec" i 0 u 1:(c+$2)  w l ls 13 t 'E0=1.65V/nm; Ns=30, Nk=102, Nt=500k'

set output "Conv1.65_T.png"
p "h2_30_142_1000k.rec" i 0  w l ls 11 t 'E0=1.65V/nm; Ns=30, Nk=142, Nt=1000k',\
  "h2_30_142_500k.rec"  i 0  w l ls 12 t 'E0=1.65V/nm; Ns=30, Nk=142, Nt=500k'



set output "Conv2.11_S.png"
p "h3_40_142_500k.rec" i 0  w l ls 11 t 'E0=2.11V/nm; Ns=40, Nk=142, Nt=500k',\
  "h3_30_142_500k.rec" i 0  w l ls 12 t 'E0=2.11V/nm; Ns=30, Nk=142, Nt=500k',\
  "h3_20_142_500k.rec" i 0  w l ls 13 t 'E0=2.11V/nm; Ns=20, Nk=142, Nt=500k'

set output "Conv2.11_K.png"
p "h3_30_182_500k.rec" i 0 u 1:(a+$2)  w l ls 11 t 'E0=2.11V/nm; Ns=30, Nk=182, Nt=500k',\
  "h3_30_142_500k.rec" i 0 u 1:(b+$2)  w l ls 12 t 'E0=2.11V/nm; Ns=30, Nk=142, Nt=500k',\
  "h3_30_102_500k.rec" i 0 u 1:(c+$2)  w l ls 13 t 'E0=2.11V/nm; Ns=30, Nk=102, Nt=500k'

set output "Conv2.11_T.png"
p "h3_30_142_1000k.rec" i 0  w l ls 11 t 'E0=2.11V/nm; Ns=30, Nk=142, Nt=1000k',\
  "h3_30_142_500k.rec" i 0   w l ls 12 t 'E0=2.11V/nm; Ns=30, Nk=142, Nt=500k',\
  "h3_30_142_300k.rec" i 0   w l ls 13 t 'E0=2.11V/nm; Ns=30, Nk=142, Nt=300k'

set output "HHG.png"
p "h1_30_142_500k.rec" i 0  w l ls 12 t 'E0=0.87V/nm',\
  "h2_30_142_500k.rec" i 0  w l ls 15 t 'E0=1.65V/nm',\
  "h3_30_142_500k.rec" i 0  w l ls 11 t 'E0=2.11V/nm'

set output "hhgVsE0.png"
set pm3d map
set yrange [0:80]
set cbrange [-25:0]
set xlabel "E-field Amplitude(V/Angstrom)"
set ylabel "Energy(eV)"
sp "hhgVsE0_200_30_142_300k.rec" w pm3d

reset
set term "qt" size 1920,1080 font 'Helvetica,25' lw 2
replot
