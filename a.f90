program SolidHHGSimpleModel
!use MPI_MDL
use UnitAndConstantMDL
use GlobalMDL
use InputOutputMDL

use LaserFieldMDL

implicit none
real(kind=dp) :: tmp_dble
real(kind=dp), allocatable :: tmp_dbleA(:), tmp_A(:)

!call LaserFieldMDL_Tester()
!stop

call PrintWallClockTime('Program starts at')

call system(sys)

call grid(gr)

call ExternalPotential(ExPo, gr)

!if(gr%dim .ne. 1) call RunPotentialOptimization(gr, sys, ExPo, grn)

call RunGroundState(ExPo, gr, grn, st, sys)

allocate(tmp_dbleA(gr%dim))
allocate(tmp_A(gr%dim))
tmp_A = 0.d0

call EvaluateMechanicalMomentum(gr, sys, st%psi, tmp_A, tmp_dbleA); print*, 'Mechanical Momentum', tmp_dbleA

call EvaluateEnergy(ExPo, gr, sys, st%psi, tmp_A, tmp_dble); print*, 'E', tmp_dble/u_eV, '(Direct)'

call CheckNormalization(gr, sys, st%psi, tmp_dble); print*, 'Norm before TD', tmp_dble

!print*, 'stop at 32 in a.f90'
!stop

call PrintWallClockTime('TD starts at')
call RunTimeDependent(ExPo, gr, obsv, st, sys, td)
call PrintWallClockTime('TD ends at')

call CheckNormalization(gr, sys, st%psi, tmp_dble); print*, 'Norm after TD', tmp_dble

call HHG_spectrum(gr, obsv, td, 'A', 1, anls)

print*, 'Running time-frequency analysis...'
call TimeFrequencyAnalysis(anls, td, sys, obsv, 'V', 1)

call PrintWallClockTime('Program ends at')

contains

  subroutine PrintWallClockTime(Text)
    implicit none
    character(len=*) :: Text
    write(*, *) Text
    call EXECUTE_COMMAND_LINE('date')
    write(*,'(A)'), '============================'
  end subroutine PrintWallClockTime

end program
