module GlobalMDL
use ParameterMDL
use VisitWriterMDL
use LaserFieldMDL
use InputOutputMDL
implicit none

!> Layer 0 type(only use intrinsic types)
type analysis_t
  integer :: n_hhg
  real(kind=dp) :: hhg_EL
  real(kind=dp) :: hhg_EU
end type analysis_t

type ExternalPotential_t
  integer :: choice
  integer :: FourierMax
  real(kind=dp), allocatable :: Fourier(:,:)
  real(kind=dp), allocatable :: V0(:)
  real(kind=dp), allocatable :: V_static(:)
end type ExternalPotential_t

type ground_t
  integer :: SolMethod !< The way to solve the eigenvalue problem.
  integer :: n_solve !< The total number of eigen-state we want to solve.
  integer :: n_BandCut !< The number of point along the band cut
  real(kind=dp), allocatable :: ev(:,:) !< The eigenvalues
  real(kind=dp), allocatable :: k_BandCutStart(:) !< Crystal momentum(in unit of 2pi/L) of the start point for band cut plot
  real(kind=dp), allocatable :: k_BandCutEnd(:) ! Crystal momentum(in unit of 2pi/L) of the end point for band cut plot
end type ground_t

type observable_t
  integer :: NtEvaluation
  integer :: EvaluationInterval
  integer :: NumberOfBandForProjection
  integer :: SolveMethodForBandProjection
  integer, allocatable :: EvGr2TmGr(:) ! Index conversion from Evaluation grid to time grid. Namely, EvGr2TmGr(i) gives the corresponding time grid for ith evaluation grid.
  real(kind=dp), allocatable :: energy(:)
  real(kind=dp), allocatable :: MechanicalMomentum(:, :)
  real(kind=dp), allocatable :: norm(:)
  real(kind=dp), allocatable :: Occupation(:, :)
end type observable_t

type state_t
  complex(kind=dp), allocatable :: psi(:,:,:)
end type state_t

type td_t
  integer :: LaserChoice
  integer :: nt
  integer :: PropMethod
  integer :: ExpOrder
  real(kind=dp) :: dt
  real(kind=dp) :: T_Pro
  real(kind=dp), allocatable :: time(:)
  real(kind=dp) :: T_Pul
  real(kind=dp) :: T_PulCen
  real(kind=dp) :: lambda
  real(kind=dp) :: omega
  real(kind=dp) :: period
  real(kind=dp), allocatable :: E0(:)
  real(kind=dp), allocatable :: field(:)
end type td_t

type xyz_t
  real(kind=dp), allocatable :: xyz(:)
end type xyz_t

!> Layer 1 type( use intrinsic type and those from layer 0)
type grid_t
  logical, allocatable :: IsPeriodic(:)
  integer, allocatable :: BoundaryCondition(:)
  integer :: dim
  integer :: NrTotal !< The total degree of freedom = Nrx*Nry*Nrz.
  integer :: NkTotal !< The total degree of freedom = Nkx*Nky*Nkz.
  integer :: n_stencil !< The number of point in a dimenion for the stencil.
  integer :: n_SS !< The number of point on one side of centre of the stencil for a dimension.
  integer, allocatable :: Nr(:)
  integer, allocatable :: Nk(:)
  type(xyz_t), allocatable :: r(:)
  type(xyz_t), allocatable :: k(:)
  type(xyz_t), allocatable :: wr(:)
  type(xyz_t), allocatable :: wk(:)
  real(kind=dp), allocatable :: boxL(:)
  real(kind=dp), allocatable :: dr(:)
  real(kind=dp), allocatable :: dk(:)
  real(kind=dp), allocatable :: RecLV(:)
  real(kind=dp), allocatable :: RShift(:)
  real(kind=dp), allocatable :: D1(:, :)
  real(kind=dp), allocatable :: D2(:, :)
end type grid_t

!> Layer 2 type(use intrinsic type and those from layer 1 and below.)
type system_t
  !type(grid_t) :: gr
  !type(ground_t) :: grn
  !type(td_t) :: td
  integer :: n_state
  !integer :: n_hhg
  !real(kind=dp) :: hhg_EL
  !real(kind=dp) :: hhg_EU
end type system_t

!type plot_t
!  type(xyz_t), allocatable :: Dom(:)
!  type(xyz_t), allocatable :: Ran(:)
!end type plot_t

type(analysis_t) :: anls
type(grid_t) :: gr
type(ground_t) :: grn
type(ExternalPotential_t) :: ExPo
type(observable_t) :: obsv
type(state_t) :: st
type(system_t) :: sys
type(td_t) :: td

!type(plot_t) :: plot



!!!! 1D begin XXXXXXXXXXXXXXXXXXXXXXXXXX
real(kind=dp), allocatable :: field_vec(:,:)
real(kind=dp), allocatable :: field_vec_mid(:,:)
real(kind=dp), allocatable :: Momentum(:)
real(kind=dp), allocatable :: Current(:)
!!!! 1D end XXXXXXXXXXXXXXXXXXXXXXXXXX

private 
! subroutine
public :: CheckNormalization, ExternalPotential, Grid, IndConv, Stencil1D, System, RunGroundState, RunPotentialOptimization, RunTimeDependent
public :: EvaluateMechanicalMomentum, EvaluateEnergy, HHG_spectrum, TimeFrequencyAnalysis
! variable
public :: anls, gr, grn, ExPo, obsv, st, sys, td

contains 


  !---------------------------------------------------------------
  !> Check if a given integer is even or not.
  function IsEvenNumber(input)
    implicit none
    logical :: IsEvenNumber
    integer, intent(in) :: input
      
    IsEvenNumber = .false.
    if(mod(input, 2) == 0) IsEvenNumber = .true.

  end function IsEvenNumber

  !---------------------------------------------------------------
  !> Check if a given integer is odd or not.
  function IsOddNumber(input)
    implicit none
    logical :: IsOddNumber
    integer, intent(in) :: input
      
    IsOddNumber = .false.
    if(mod(input, 2) .ne. 0) IsOddNumber = .true.

  end function IsOddNumber

  !---------------------------------------------------------------
  !> Given an integer with periodicity N, it returns the next integer within 1~N.
  function NextPeriodicNumber(i_now, N)
    implicit none
    integer :: NextPeriodicNumber
    integer, intent(in) :: i_now
    integer, intent(in) :: N
    if ((i_now < 1) .or. (i_now > N)) call MessageText('Error','S:RunGroundState:: NextPeriodicNumber only accept input 1~N.')
    if (N <= 0) call MessageText('Error','S:RunGroundState:: N must  > 0 in NextPeriodicNumber.')
    if (i_now + 1 <= N) then
      NextPeriodicNumber = i_now + 1
    else
      NextPeriodicNumber = 1
    end if
  end function NextPeriodicNumber

  !---------------------------------------------------------------
  !> Periodizing index: given any integer with periodicity N, return corresponding integer withing 1~N or 0~N-1. Default is 1~N.
  function Prdz(input, periodicity, ZeroOrOneConv)
    implicit none
    Integer :: Prdz
    integer, intent(in) :: input, periodicity
    character(len = *), optional, intent(in) :: ZeroOrOneConv

    ! Internal
    integer :: beg

    beg = 1
    if(present(ZeroOrOneConv) .and. (ZeroOrOneConv == 'Zero' .or. ZeroOrOneConv == 'zero')) beg = 0

    Prdz = mod(input, periodicity)
    if (Prdz < 0) Prdz = Prdz + periodicity
    if((Prdz == 0) .and. (beg == 1)) Prdz = periodicity

  end function Prdz

  !---------------------------------------------------------------
  !> Note this must be called within OpenMP environment.
  subroutine CalculateBandProjection(ExPo, gr, st, sys, NumberOfBandForProjection, SolveMethod, A, Occupation)
    implicit none
    type(ExternalPotential_t), intent(in) :: ExPo
    type(grid_t), intent(in) :: gr
    type(state_t), intent(in) :: st
    type(system_t), intent(in) :: sys
    integer, intent(in) :: NumberOfBandForProjection
    integer, intent(in) :: SolveMethod
    real(kind=dp), intent(in) :: A(gr%dim)
    real(kind=dp), intent(out) :: Occupation(NumberOfBandForProjection, gr%NkTotal)

    ! Internal
    integer, parameter :: ProgressInterval = 10
    integer :: i, j, l
    integer :: i_dim
    integer :: IndexS
    integer :: ll(gr%dim)
    integer :: IndexM(gr%dim)
    real(kind=dp) :: k_sub(gr%dim)
    real(kind=dp) :: eigenvalue(NumberOfBandForProjection)
    complex(kind=dp) :: OccupationForEachParticleAtK(NumberOfBandForProjection, sys%n_state)
    complex(kind=dp) :: eigenstate(gr%NrTotal, NumberOfBandForProjection)

    ! 1D plot for band distribution
    logical :: IsPlotting = .false.
    integer, parameter :: N_Energy = 500
    integer :: FirstBZ_OutMark
    integer :: FirstBZ_OutType
    real(kind=dp), parameter :: Energy_upper = 20.d0*u_eV
    real(kind=dp), parameter :: Energy_lower = -20.d0*u_eV
    real(kind=dp), parameter :: GaussianStandardDeviation = 0.5d0*u_eV/2.355d0 !2.355 is the factor for conversion between standard deviation and FWHM 
    real(kind=dp) :: Energy(N_Energy)
    real(kind=dp) :: BandDistribution(N_Energy, gr%Nk(1))
    real(kind=dp) :: tp(50), wp(50)
    real(kind=dp) :: gaussian(N_Energy)
    real(kind=dp) :: k_periodic
    real(kind=dp) :: k_tmp(gr%Nk(1))

    ! Writing unit
    integer :: WU

    do l = 1, gr%NkTotal

      ! Progress printing
      !if(OMP_GET_THREAD_NUM() == 0) then
      !  if(mod(l, progress_interval) == 1) print*, 'Ground state solving; current, chunk, and total k points:', l, lfi-lst, gr%NkTotal
      !end if 

      ! Specify specific crystal momentum for the sub-Hamiltonian
      IndexS = l
      call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
      ll=IndexM

      do i_dim = 1, gr%dim
        k_sub(i_dim) = gr%k(i_dim)%xyz(ll(i_dim))  
      end do

      ! Solve the sub-eigenvalue problem and calculate the projections for this specific crystal momentum
      call SolveSubHamiltonian(ExPo, gr, SolveMethod, NumberOfBandForProjection, A, k_sub, eigenvalue, eigenstate)
      call ZGEMM('C', 'N', NumberOfBandForProjection, sys%n_state, gr%NrTotal, dcmplx(1.d0, 0.d0), eigenstate, gr%NrTotal, st%psi(:,:, l), gr%NrTotal, dcmplx(0.d0, 0.d0), OccupationForEachParticleAtK, NumberOfBandForProjection)       
      Occupation(:, l) = sum(abs(OccupationForEachParticleAtK)**2, 2)

      ! Prepare the band distribution if system is 1D
      if((gr%dim == 1).and.(IsPlotting)) then
        Energy = (/( (Energy_upper - Energy_lower)/dble(N_Energy-1)*dble(i-1) + Energy_lower, i = 1, N_Energy, 1 )/)
        BandDistribution(:, l) = 1.d-99 ! such that no error occurs when plotting in log scale
        do i = 1, NumberOfBandForProjection
          tp(1) = eigenvalue(i)
          wp(1) = GaussianStandardDeviation
          call WindowGenerator(N_Energy-1, 3, Energy(1:N_Energy), tp, gaussian(1:N_Energy), wp)
          BandDistribution(:, l) = BandDistribution(:, l) + gaussian*Occupation(i, l)
        end do
      end if

    end do
    ! Ploting the band distribution if system is 1D
    if((gr%dim == 1).and.(IsPlotting)) then
      open(newunit = WU, file = 'band_distribution.dat', access = 'append', status = 'old' )
      k_tmp = gr%k(1)%xyz + A(1)
      if(any(k_tmp + 0.5d0*gr%RecLV(1) <= 0.d0)) then
        FirstBZ_OutMark = minloc((k_tmp + 0.5d0*gr%RecLV(1))**2, 1) ! FirstBZ_OutMark+1 ~ nk, 1 ~ FirstBZ_OutMark
        FirstBZ_OutType = -1
      elseif(any(k_tmp - 0.5d0*gr%RecLV(1) > 0.d0)) then
        FirstBZ_OutMark = minloc((k_tmp - 0.5d0*gr%RecLV(1))**2, 1) ! FirstBZ_OutMark+1 ~ nk, 1 ~ FirstBZ_OutMark
        FirstBZ_OutType = 1
      else
        FirstBZ_OutMark = 0
        FirstBZ_OutType = 0
      end if
      do i = FirstBZ_OutMark+1, gr%Nk(1)
        k_periodic = gr%k(1)%xyz(i) + A(1)
        ! FIXME: the following periodizer will fail if k_periodic go beyond the second BZ.
        if(FirstBZ_OutType == 1) k_periodic = k_periodic - gr%RecLV(1)
        do j = 1, N_Energy
          write(WU, '(3ES)') k_periodic/gr%RecLV(1), Energy(j)/u_eV, BandDistribution(j, i)
        end do
        write(WU,*) ''
      end do
      if(FirstBZ_OutMark .ne. 0) then
        do i = 1, FirstBZ_OutMark
          k_periodic = gr%k(1)%xyz(i) + A(1)
          if(FirstBZ_OutType == -1) k_periodic = k_periodic + gr%RecLV(1)
          do j = 1, N_Energy
            write(WU, '(3ES)') k_periodic/gr%RecLV(1), Energy(j)/u_eV, BandDistribution(j, i)
          end do
          write(WU,*) ''
        end do
      end if
      write(WU,*) ''
      close(WU)
    end if


  end subroutine CalculateBandProjection

  !---------------------------------------------------------------
  !> Note this must be called within OpenMP environment.
  subroutine OpenMPIndex(n, IsThisCpuWait, i_first, i_last, Conv0or1, Report)
    use omp_lib
    implicit none
    integer, intent(in) :: n ! total number of tasks
    logical, intent(out) :: IsThisCpuWait
    integer, intent(out) :: i_first
    integer, intent(out) :: i_last
    character(len=*), optional, intent(in) :: Conv0or1
    character(len=*), optional, intent(in) :: Report

    integer :: n_cpu
    integer :: cpu
    integer :: chunk
    integer :: residual
    character(len=5) :: ConvFlag
    character(len=14) :: Report_tmp


    IsThisCpuWait = .false.
    ConvFlag = 'Con1'
    if(present(Conv0or1)) ConvFlag = trim(adjustl(Conv0or1))
    if(.not.(ConvFlag == 'Conv0' .or. ConvFlag == 'Conv1')) call MessageText('Error','F:OpenMPIndex:: Invalid character input "Conv0or1". It could only be "Conv0" or "Conv1". ')

    n_cpu = OMP_GET_NUM_THREADS()
    cpu = OMP_GET_THREAD_NUM()
    chunk = n/n_cpu
    residual = mod(n, n_cpu)

    if(cpu+1 <= residual ) then
      i_first = cpu*(chunk + 1) + 1
      i_last = i_first + chunk
    else
      i_first = cpu*(chunk) + residual + 1
      i_last = i_first + chunk - 1
      if(chunk == 0) IsThisCpuWait = .true.
    end if

    if(ConvFlag == 'Conv0') then
      i_first = i_first - 1 
      i_last = i_last - 1 
    end if

    Report_tmp = 'NoReport'
    if(present(Report)) Report_tmp =trim(adjustl(Report))
    select case(trim(adjustl(Report_tmp)))
      case ('NoReport')
        ! Do nothing
      case ('Report')
        ! FIXME: this should write data to output file monitoring the running program.
      case('ReportAndPrint')
        if(cpu == 0) write(*, '(A,1X,A,1X,A,1X,A,1X,A,1X,A,1X,A)') '# of cpu', '     tasks', '   cpu', '     chunk', '     start', '    finish', 'idle?'
        write(*, '(I8,1X,I10,1X,I6,1X,I10,1X,I10,1X,I10,1X,L5)') n_cpu, n, cpu, i_last - i_first + 1, i_first, i_last, IsThisCpuWait
      case default
      if(.not.(Report == 'NoReport' .or. Report == 'Report' .or. Report == 'ReportAndPrint')) call MessageText('Error','F:OpenMPIndex:: Invalid input "Report". It could only be "NoReport", "Report", or "ReportAndPrint". ')
    endselect

  end subroutine OpenMPIndex

  !---------------------------------------------------------------
  !> This calculate the total probability of periodic part of Bloch's wave psi for each electron and 
  !>   then calculate the average of them over all electron. Namely, (int |psi(r)|^2 dr)/(number of electron) over one space period.
  subroutine CheckNormalization(gr, sys, psi, normalization)
    implicit none
    type(grid_t),     intent(in) :: gr
    type(system_t),   intent(in) :: sys
    complex(kind=dp), intent(in) :: psi(gr%NrTotal, sys%n_state, gr%NkTotal)
    real(kind=dp), intent(out) :: normalization

    ! Interal
    integer :: i, j, l
    integer, allocatable :: ii(:)
    real(kind=dp) :: tmp
     
    allocate(ii(gr%dim))

    normalization = 0.d0
    do l = 1, gr%NkTotal
      do j = 1, sys%n_state
        do i = 1, gr%NrTotal
          tmp = abs(psi(i,j,l))
          normalization = normalization + tmp*tmp
        end do
      end do
    end do

    normalization = normalization/dble(gr%NkTotal)/sys%n_state

  end subroutine CheckNormalization

  !---------------------------------------------------------------
  !> Calculate the canonical Momentum density per cell
  subroutine EvaluateMechanicalMomentum(gr, sys, psi, A, MechanicalMomentum)
    implicit none
    type(grid_t),     intent(in)  :: gr
    type(system_t),   intent(in)  :: sys
    real(kind=dp),    intent(in)  :: A(gr%dim)
    complex(kind=dp), intent(in)  :: psi(gr%NrTotal, sys%n_state, gr%NkTotal)
    real(kind=dp),    intent(out) :: MechanicalMomentum(gr%dim)
    
    ! Internal
    integer :: i_dim, i_state, i_direction, i_stencil, i, l
    integer :: IndexS
    integer, allocatable :: ii(:), ll(:), IndexM(:)
    real(kind=dp) :: volume
    complex(kind=dp) :: tmp
    complex(kind=dp) :: psi_tmp(gr%NrTotal, sys%n_state, gr%NkTotal)

    allocate(ii(gr%dim))
    allocate(ll(gr%dim))
    allocate(IndexM(gr%dim))

    volume = 1.d0
    do i_dim = 1, gr%dim
      volume = volume*gr%BoxL(i_dim)
    end do

    MechanicalMomentum = 0.d0
    do i_dim = 1, gr%dim
      select case (i_dim)
        case(1) 
          call OperatorToBlochWaveAndRemoveExpPart(gr, sys, 'CanonicalMomentumX', A, psi, psi_tmp)
        case(2) 
          call OperatorToBlochWaveAndRemoveExpPart(gr, sys, 'CanonicalMomentumY', A, psi, psi_tmp)
        case(3) 
          call OperatorToBlochWaveAndRemoveExpPart(gr, sys, 'CanonicalMomentumZ', A, psi, psi_tmp)
      end select

      do l = 1, gr%NkTotal
        IndexS = l
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
        ll = IndexM
        do i_state = 1, sys%n_state
          ! The expression in the next line assumes each state is fully occupied(normalized to one), and the expression in the next next line does not assume full occupation(more general).
          !MechanicalMomentum(i_dim) = MechanicalMomentum(i_dim) + (gr%k(i_dim)%xyz(ll(i_dim)) + A(i_dim) + dble(dot_product(psi(:, i_state, l), psi_tmp(:, i_state, l))) )/volume ! up to here is from single-body wavefunction
          MechanicalMomentum(i_dim) = MechanicalMomentum(i_dim) + (  (gr%k(i_dim)%xyz(ll(i_dim)) + A(i_dim))*dble(dot_product(psi(:, i_state, l), psi(:, i_state, l))) + dble(dot_product(psi(:, i_state, l), psi_tmp(:, i_state, l))) )/volume ! up to here is from single-body wavefunction
        end do
      end do
      MechanicalMomentum(i_dim) = MechanicalMomentum(i_dim)/dble(gr%NkTotal) ! this is from many-body wavefunction
    end do

  end subroutine EvaluateMechanicalMomentum

  !---------------------------------------------------------------
  !> Calculate the energy density per cell
  subroutine EvaluateEnergy(Expo, gr, sys, psi, A, Energy)
    implicit none
    type(ExternalPotential_t) :: ExPo
    type(system_t), intent(in) :: sys
    type(grid_t), intent(in) :: gr
    real(kind=dp), intent(in) :: A(gr%dim)
    complex(kind=dp), intent(in) :: psi(gr%NrTotal, sys%n_state, gr%NkTotal)
    real(kind=dp), intent(out) :: Energy

    complex(kind=dp) :: psi_tmp(gr%NrTotal, sys%n_state, gr%NkTotal)
    
    ! Internal
    integer :: i_dim, i_state, l
    integer :: IndexS
    integer, allocatable :: ll(:), IndexM(:)
    real(kind=dp) :: volume

    allocate(ll(gr%dim))
    allocate(IndexM(gr%dim))

    volume = 1.d0
    do i_dim = 1, gr%dim
      volume = volume*gr%BoxL(i_dim)
    end do

    call OperatorToBlochWaveAndRemoveExpPart(gr, sys, 'Hamiltonian', A, psi, psi_tmp, ExPo)
    Energy = 0.d0
    do l = 1, gr%NkTotal
      IndexS = l
      call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
      ll = IndexM
      do i_state = 1, sys%n_state
        Energy = Energy + dble(dot_product(psi(:, i_state, l), psi_tmp(:, i_state, l)))/volume ! up to here it's a single-body wavefunction ! FIXME: check if energy evaluation assumes full occupation.
      end do
    end do
    Energy = Energy/gr%NkTotal !this comes from the averaging over cells.

  end subroutine EvaluateEnergy

  !---------------------------------------------------------------
  subroutine ExternalPotential(ExPo, gr)
    implicit none
    type(ExternalPotential_t), intent(inout) :: ExPo
    type(grid_t), intent(in) :: gr

    ! Internal
    integer :: i, j, i_dim
    integer :: IndexS
    integer :: n_SuperLattice
    integer, allocatable :: IndexM(:)
    real(kind=dp), allocatable :: density(:, :)
    real(kind=dp) :: tmp
    real(kind=dp) :: impurity_relative_strength

    ! WU
    integer :: WU_ext
    open(newunit = WU_ext, file = 'V_ext.dat')

    call Parse('ExternalPotentialChoice', ExPo%choice)
    
    allocate(ExPo%V0(gr%dim))
    call Parse('ExternalPotentialHeight', gr%dim, ExPo%V0)

    ! Calculating the value of the external potential at each grid point.
    allocate(ExPo%V_static(gr%NrTotal))
    allocate(IndexM(gr%dim))
    select case (ExPo%choice)
    case (1) ! Mathieu-type potential: -V0x(1+cos(X)) + -V0y(1+cos(Y))
      ExPo%V_static = 0.d0
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        do i_dim = 1, gr%dim
          ExPo%V_static(i) = ExPo%V_static(i) - ExPo%V0(i_dim)*(1.d0 + cos(gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim))))
        end do
      end do
    case (2) ! Inseparable Mathieu-type potentail: -V0(1+cos(X))(1+cox(Y))
      ExPo%V_static = 1.d0
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        do i_dim = 1, gr%dim
          ExPo%V_static(i) = ExPo%V_static(i)*(1.d0 + cos(gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim))))
        end do
        ExPo%V_static(i) = -ExPo%V_static(i)*ExPo%V0(1)
      end do
    case (3) ! V0V(X)V(Y), V(X) and V(Y) are symmetric functions and expressed with real Fourier components(real due to symmetry)
      ExPo%V_static = 1.d0
      call Parse('ExternalPotentialFourierMax', ExPo%FourierMax)
      if(ExPo%FourierMax <= 0)call MessageText('Error','S:ExternalPotential:: The parameter "ExternalPotentialFourierMax" must be >= 1.')
      allocate(ExPo%Fourier(0: ExPo%FourierMax, gr%dim))
      do i_dim = 1, gr%dim
        if(i_dim == 1) call Parse('ExternalPotentialFourierCompX', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
        if(i_dim == 2) call Parse('ExternalPotentialFourierCompY', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
        if(i_dim == 3) call Parse('ExternalPotentialFourierCompZ', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
      end do
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        do i_dim = 1, gr%dim
          tmp = ExPo%Fourier(0, i_dim)
          do j = 1, ExPo%FourierMax
            tmp = tmp + ExPo%Fourier(j, i_dim)*2.d0*cos( dble(j)*gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim)) )
          end do 
          ExPo%V_static(i) = ExPo%V_static(i)*tmp
        end do
        ExPo%V_static(i) = ExPo%V_static(i)*ExPo%V0(1)
      end do
    case (4) ! V0X*V(X)+ V0Y*V(Y), V(X) and V(Y) are symmetric functions and expressed with real Fourier components(real due to symmetry)
      ExPo%V_static = 0.d0
      call Parse('ExternalPotentialFourierMax', ExPo%FourierMax)
      if(ExPo%FourierMax <= 0)call MessageText('Error','S:ExternalPotential:: The parameter "ExternalPotentialFourierMax" must be >= 1.')
      allocate(ExPo%Fourier(0: ExPo%FourierMax, gr%dim))
      do i_dim = 1, gr%dim
        if(i_dim == 1) call Parse('ExternalPotentialFourierCompX', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
        if(i_dim == 2) call Parse('ExternalPotentialFourierCompY', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
        if(i_dim == 3) call Parse('ExternalPotentialFourierCompZ', ExPo%FourierMax + 1, ExPo%Fourier(0:ExPo%FourierMax, i_dim))
      end do
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        do i_dim = 1, gr%dim
          tmp = ExPo%Fourier(0, i_dim)
          do j = 1, ExPo%FourierMax
            tmp = tmp + ExPo%Fourier(j, i_dim)*2.d0*cos( dble(j)*gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim)) )
          end do 
          ExPo%V_static(i) = ExPo%V_static(i) + ExPo%V0(i_dim)*tmp
        end do
      end do
    case (5) ! Mathieu-type potential: -V0x(1+cos(X)) + -V0y(1+cos(Y)) with impurity(spike potential) and superlattice
      if(gr%dim .ne. 1) call MessageText('Error','S:ExternalPotential:: For choice "5" current program only support 1 dimension system.')
      call Parse('ExternalPotentialNumberOfSuperLattice', n_SuperLattice)
      call Parse('ImpurityRelativeStrength', impurity_relative_strength)
      ExPo%V_static = 0.d0
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        do i_dim = 1, gr%dim
          ExPo%V_static(i) = ExPo%V_static(i) - ExPo%V0(i_dim)*(1.d0 + cos(dble(n_SuperLattice)*gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim))))
          do j = 1, n_SuperLattice
            ExPo%V_static(i) = ExPo%V_static(i) + impurity_relative_strength*ExPo%V0(i_dim)/dble(n_SuperLattice)*cos(dble(j)*gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim)))
          end do
        end do
      end do
    case default
      call MessageText('Error','S:ExternalPotential:: Currently "choice" for external potential can only be 1.')
    end select

    ! The way of data writing here should be refined in the future.
    select case (gr%dim)
    case (1)
      call WriteDataCurve2D_DoubleReal(WU_ext, 'External Potential', gr%Nr(1), ExPo%V_static(1:gr%Nr(1)), gr%r(1)%xyz, 1.d0, 1.d0, 0, gr%Nr(1))
    case (2)
      allocate(density(gr%Nr(1),gr%Nr(2)))
      do i =1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        density(IndexM(1), IndexM(2)) = ExPo%V_static(i)
        write(WU_ext, *) gr%r(1)%xyz(IndexM(1)), gr%r(2)%xyz(IndexM(2)), ExPo%V_static(i)  !direct writing
        if(mod(i,gr%Nr(1)) == 0) write(WU_ext, *) ''                                       !direct writing
      end do
      !call WriteDataVTK_2D_Density_DoubleReal(WU_ext, 'External Potential', density, gr%Nr(1), gr%Nr(2), gr%r(1)%xyz, gr%r(2)%xyz, gr%Nr(1), gr%Nr(2), 1.d0, 1.d0)
    case default
      call MessageText('Warning','S:ExternalPotential:: Currently plotting external potential with dimension =3 is not supported.')
    end select 

  end subroutine ExternalPotential

  !---------------------------------------------------------------
  !>
  !! This subroutine allocate and build up the grid points.
  subroutine Grid(gr)
    implicit none
    type(grid_t), intent(inout) :: gr
    integer :: i, j, i_dim

    ! Read/check the dimension of the system.
    call Parse('MaxDimension', gr%dim)

    if((gr%dim < 1) .or. (gr%dim > 3) ) then
      call MessageText('Error','S:Grid:: The dimension of a system must be 1, 2, or 3.')
    end if


    ! Read/check the boundary conditions.
    allocate(gr%BoundaryCondition(gr%dim))
    allocate(gr%IsPeriodic(gr%dim))

    call Parse('BoundaryCondition', gr%dim, gr%BoundaryCondition)

    do i=1, gr%dim
      if(gr%BoundaryCondition(i) == 0) then
        gr%IsPeriodic(i) = .false.
      else if(gr%BoundaryCondition(i) == 1) then
        gr%IsPeriodic(i) = .true.
      else
        call MessageText('Error','S:Grid:: The "BoundaryCondition" can only be 0 or 1.')
      end if
    end do


    ! Read/check simulation box size, number of grid point in each direction, and
    !   the shift in real space.
    allocate(gr%nr(gr%dim))
    allocate(gr%nk(gr%dim))
    allocate(gr%BoxL(gr%dim))

    call Parse('Nr', gr%dim, gr%Nr)
    call Parse('Nk', gr%dim, gr%Nk)
    call Parse('BoxAxisLength', gr%dim, gr%BoxL)

    if(any(gr%nr <= 1)) call MessageText('Error','S:Grid:: The number of grid point in a dimension must be > 1.')
    if(any(gr%boxL <= 0.d0)) call MessageText('Error','S:Grid:: The length of any simulation axis must be > 0.')
    do i = 1, gr%dim
      if(gr%IsPeriodic(i) == .false. ) then
        if (gr%Nk(i) .ne. 1) call MessageText('Error','S:Grid:: The number of grid point in k should be 1 if that direction is non-periodic.')
      end if
    end do

    gr%NrTotal = 1
    gr%NkTotal = 1
    do i = 1, gr%dim
      gr%NrTotal = gr%NrTotal*gr%Nr(i)
      gr%NkTotal = gr%NkTotal*gr%Nk(i) !FIXME: add a check that when having non-periodic boundary condition, Nk is set to 1. 
    end do

    allocate(gr%RecLV(gr%dim))
    gr%RecLV = 2.d0*pi/gr%BoxL


    ! Construct the values at each grid point.
    ! For periodic direction, the last point is not taken as it's the same as the first point.
    ! For non-periodic direction, the last point is taken, and the value of k is always set to gamma point.
    allocate(gr%r(gr%dim))
    allocate(gr%k(gr%dim))
    allocate(gr%dr(gr%dim)) 
    allocate(gr%dk(gr%dim)) 
    allocate(gr%wr(gr%dim))
    allocate(gr%wk(gr%dim))
    allocate(gr%RShift(gr%dim))

    call Parse('RealSpaceShift', gr%dim, gr%Rshift)
    
    do i = 1, gr%dim

      ! Real space grid
      allocate(gr%r(i)%xyz(gr%Nr(i)))
      allocate(gr%wr(i)%xyz(gr%Nr(i)))

      gr%dr(i) = gr%BoxL(i)/dble(gr%Nr(i) - 1)
      if(gr%IsPeriodic(i)) gr%dr(i) = gr%BoxL(i)/dble(gr%Nr(i))

      gr%r(i)%xyz(1:gr%Nr(i)) = (/ (dble(j-1), j = 1, gr%Nr(i)) /)
      gr%r(i)%xyz = gr%r(i)%xyz*gr%dr(i) + gr%RShift(i) - 0.5d0*gr%BoxL(i) + 0.5d0*gr%dr(i) !FIXME: corresponding print for periodic boundary condition has not yet been fixed.

      if(any(gr%Nr < 3)) call MessageText('Error','S:Grid:: The number of points in real space in any direction  should be larger than 3(for calculating derivatives).')

      gr%wr(i)%xyz = 1.d0

      ! K space grid
      allocate(gr%k(i)%xyz(gr%Nk(i)))
      allocate(gr%wk(i)%xyz(gr%Nk(i)))
      ! FIXME: check if Bloch wave really allows such k grid distribution.

      gr%dk(i) = gr%RecLV(i)/dble(gr%Nk(i))

      if( IsOddNumber(gr%Nk(i))) then
        gr%k(i)%xyz(:) = (/ (dble(j)*gr%dk(i)     , j = -(gr%Nk(i) - 1)/2, (gr%Nk(i) - 1)/2, 1) /)
      else
        gr%k(i)%xyz(:) = (/ (dble(j)/2.d0*gr%dk(i), j = -(gr%Nk(i) - 1)  , (gr%Nk(i) - 1)  , 2) /)
      end if

      gr%wk(i)%xyz = 1.d0

      if(.not.gr%IsPeriodic(i)) then
        gr%k(i)%xyz = 0.d0 ! set k to Gamma point
        gr%wr(i)%xyz(1) = 0.5d0 ! first point only has half weight in integration.
        gr%wr(i)%xyz(gr%Nr(i)) = 0.5d0 ! last point only has half weight in integration.
      end if

    end do 


    ! Read/check number of point in the stencil, and perpare the stencil for 1st and 2nd derivative
    call Parse('N_Stencil', gr%n_stencil)

    if((gr%n_stencil/2  == 0) .or. (gr%n_stencil < 3)) call MessageText('Error','S:Grid:: The number of point in the stencil should be positive odd number and >=3.')
    if(any(gr%Nr < gr%n_stencil)) call MessageText('Error','S:Grid:: The number of points in stencil should be larger than the number of grid in any direction.')

    gr%n_SS = (gr%n_stencil - 1)/2

    allocate(gr%D1(-gr%n_SS:gr%n_SS, gr%dim))
    allocate(gr%D2(-gr%n_SS:gr%n_SS, gr%dim))

    do i_dim = 1, gr%dim
      call Stencil1D(gr%n_stencil, 1, gr%r(i_dim)%xyz((gr%n_stencil+1)/2), gr%r(i_dim)%xyz(1:gr%n_stencil), gr%D1(-gr%n_SS:gr%n_SS, i_dim))
      call Stencil1D(gr%n_stencil, 2, gr%r(i_dim)%xyz((gr%n_stencil+1)/2), gr%r(i_dim)%xyz(1:gr%n_stencil), gr%D2(-gr%n_SS:gr%n_SS, i_dim))
    end do 


    ! Print information
    Write(*,'(A,1X,I)') 'Dimension:', gr%dim

    Write(*,'(A)') 'Number of point in real space for'
    do i = 1, gr%dim
      Write(*,'(A,1X,I,1X,A,1X,I)') '     ', i, 'th dimension:', gr%Nr(i)
    end do

    Write(*,'(A)') 'Number of point in k space for'
    do i = 1, gr%dim
      Write(*,'(A,1X,I,1X,A,1X,I)') '     ', i, 'th dimension:', gr%Nk(i)
    end do

    Write(*,'(A)') 'Grid spacing in real space for'
    do i = 1, gr%dim
      Write(*,'(A,1X,I,1X,A,1X,F)') '     ', i, 'th dimension:', gr%dr(i)
    end do

    Write(*,'(A)') 'Grid spacing in k space for'
    do i = 1, gr%dim
      Write(*,'(A,1X,I,1X,A,1X,F)') '     ', i, 'th dimension:', gr%dk(i)
    end do

    write(*,'(A,1X,I)') 'The total number of points in a stencil in one dimension:', gr%n_stencil

  end subroutine Grid

  !---------------------------------------------------------------
  ! This subroutine gives a vector resulting from applying an operator to a Bloch's wave 
  !   and then removing the exponential part of the operated Bloch wave. Namely, 
  !   result = {e^(-ikr)} x Operator x {e^(ikr) phi_k(r)}, where phi_k(r) is a state in sub-Hilbert space H(k).
  ! In addition, not only one but all electrons' waves are calculated, and output as a total vector.
  subroutine OperatorToBlochWaveAndRemoveExpPart(gr, sys, NameOfOperator, A, psi_in, psi_out, ExPo)
    use omp_lib
    implicit none
    type(grid_t),              intent(in) :: gr
    type(system_t),            intent(in) :: sys
    character(len=*),          intent(in) :: NameOfOperator
    real(kind=dp),             intent(in) :: A(gr%dim)
    complex(kind=dp),          intent(in) :: psi_in(gr%NrTotal, sys%n_state, gr%NkTotal)
    complex(kind=dp),          intent(out) :: psi_out(gr%NrTotal, sys%n_state, gr%NkTotal)
    type(ExternalPotential_t), intent(in), optional :: ExPo ! FIXME: the whole subroutine should be changed to interface form for taking different Operator.
 
    ! internal
    integer :: i, i_dim, i_stencil, i_state, i_direction, j, l
    integer :: IndexS
    integer, allocatable :: ii(:), jj(:), ll(:)
    integer, allocatable :: IndexM(:)
    complex(kind=dp) :: tmp

    ! parallel
    logical :: IsThisCpuWait
    integer :: lst, lfi

    allocate(ii(gr%dim))
    allocate(jj(gr%dim))
    allocate(ll(gr%dim))
    allocate(IndexM(gr%dim))

    if ((trim(adjustl(NameOfOperator)) == 'CanonicalMomentumX') .or. &
       &(trim(adjustl(NameOfOperator)) == 'CanonicalMomentumY') .or. &
       &(trim(adjustl(NameOfOperator)) == 'CanonicalMomentumZ')) then
      select case (trim(adjustl(NameOfOperator)))
        case('CanonicalMomentumX')
          i_direction = 1
        case('CanonicalMomentumY')
          i_direction = 2
        case('CanonicalMomentumZ')
          i_direction = 3
      end select
      if(i_direction > gr%dim) call MessageText('Error','S:OperatorToBlochWaveAndRemoveExpPart:: The requested dimension for  mechanical momentum is beyond the system.')
      !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(IsThisCpuWait, lst, lfi, l, i, ii, j, jj, IndexS, IndexM, tmp, i_stencil, i_state)
      call OpenMPIndex(gr%NkTotal, IsThisCpuWait, lst, lfi, 'Conv1', 'Report')
      do l = lst, lfi
!      do l = 1, gr%NkTotal
        do i_state = 1, sys%n_state
          do i = 1, gr%NrTotal 
            IndexS = i
            call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
            ii = IndexM
            tmp = dcmplx(0.d0, 0.d0)
            do i_stencil = -gr%n_SS, gr%n_SS
              jj = ii
              jj(i_direction) = Prdz(jj(i_direction) + i_stencil, gr%Nr(i_direction))
              IndexM = jj
              call IndConv('M2S', IndexM, IndexS, gr%dim, gr%Nr)
              j = IndexS
              tmp = tmp + dcmplx(0.d0, -gr%D1(i_stencil, i_direction))*psi_in(j, i_state, l)
            end do
            psi_out(i, i_state, l) = tmp
          end do
        end do
      end do
      !$OMP END PARALLEL
    elseif (trim(adjustl(NameOfOperator)) == 'Hamiltonian') then
      if(.not. present(ExPo)) call MessageText('Error','S:OperatorToBlochWaveAndRemoveExpPart:: External potential must be specified when Operator applied is Hamiltonian".')
      !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(IsThisCpuWait, lst, lfi, l, ll, i, ii, j, jj, IndexS, IndexM, tmp, i_dim, i_stencil, i_state)
      call OpenMPIndex(gr%NkTotal, IsThisCpuWait, lst, lfi, 'Conv1', 'Report')
      do l = lst, lfi
!      do l = 1, gr%NkTotal
        IndexS = l
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
        ll = IndexM
        do i_state = 1, sys%n_state
          do i = 1, gr%NrTotal 
            IndexS = i
            call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
            ii = IndexM
            tmp = dcmplx(0.d0, 0.d0)
            ! External potential part
            tmp = tmp + dcmplx(ExPo%V_static(i), 0.d0)*psi_in(i, i_state, l)
            do i_dim = 1, gr%dim
              ! {(k^2+A^2)/2 + k dot A} part
              tmp = tmp + dcmplx(  0.5d0*gr%k(i_dim)%xyz(ll(i_dim))**2.d0 &
                                &+ 0.5d0*A(i_dim)**2.d0 &
                                &+ gr%k(i_dim)%xyz(ll(i_dim))*A(i_dim)&
                                &, 0.d0)*psi_in(i, i_state, l)
              ! {-0.5 Laplacian -i(k+A) dot grad} part
              do i_stencil = -gr%n_SS, gr%n_SS
                jj = ii
                jj(i_dim) = Prdz(jj(i_dim) + i_stencil, gr%Nr(i_dim))
                IndexM = jj
                call IndConv('M2S', IndexM, IndexS, gr%dim, gr%Nr)
                j = IndexS
                tmp = tmp + ( dcmplx(-0.5d0*gr%D2(i_stencil, i_dim), 0.d0) +&
                            & dcmplx(0.d0, -(gr%k(i_dim)%xyz(ll(i_dim)) + A(i_dim))*gr%D1(i_stencil, i_dim))&
                           &)*psi_in(j, i_state, l)
              end do
            end do
            psi_out(i, i_state, l) = tmp
          end do
        end do
      end do
      !$OMP END PARALLEL
    else
      call MessageText('Error','S:OperatorToBlochWaveAndRemoveExpPart:: NameOfOperator only supports "CanonicalMomentum[X~Z]" and "Hamiltonian".')
    end if

  end subroutine OperatorToBlochWaveAndRemoveExpPart

  !---------------------------------------------------------------
  subroutine RunGroundState(ExPo, gr, grn, st, sys)
    use omp_lib
    implicit none
    type(ExternalPotential_t) :: ExPo
    type(grid_t), intent(in) :: gr
    type(ground_t), intent(inout) :: grn
    type(state_t), intent(inout) :: st
    type(system_t), intent(inout) :: sys

    ! Internal
    integer, parameter :: progress_interval = 10
    integer :: i, j, l, hst, hfi, cst, cfi, jjj, i_dim, i_stencil
    integer :: IndexS
    integer, allocatable :: ii(:), jj(:), ll(:)
    integer, allocatable :: IndexM(:)
    real(kind=dp) :: BandCutKtmp(gr%dim)
    real(kind=dp) :: BandCutDk(gr%dim)
    real(kind=dp) :: normalization
    real(kind=dp) :: effective_mass
    real(kind=dp) :: A(gr%dim)
    real(kind=dp) :: k_sub(gr%dim)
    real(kind=dp), allocatable :: coefD2(:)
    real(kind=dp), allocatable :: band2Dplot(:,:)
    real(kind=dp), allocatable :: k_effective_mass(:)
    real(kind=dp), allocatable :: BandCutK(:)
    real(kind=dp), allocatable :: BandCutStructure(:,:)
    complex(kind=dp), allocatable :: eigenstate(:,:)

    ! for OpenMP
    logical :: IsThisCpuWait
    integer :: lst, lfi

    ! Writer unit
    integer :: WU_BandCut, WU_ev, WU_ini, WU_KaneComparison

    open(newunit = WU_BandCut, file = 'band_cut.dat')
    open(newunit = WU_ev, file = 'band.dat')
    open(newunit = WU_ini, file = 'state0.dat')
    open(newunit = WU_KaneComparison, file = 'band_kane.dat')


    call Parse('GroundSolveMethod', grn%SolMethod)
    call Parse('GroundSolveState', grn%n_solve)
    allocate(grn%ev(grn%n_solve,gr%NkTotal))
    allocate(eigenstate(gr%NrTotal, grn%n_solve))

    ! Preparation for index conversion
    allocate(IndexM(gr%dim))
    allocate(ii(gr%dim))
    allocate(jj(gr%dim))
    allocate(ll(gr%dim))

    ! Allocate the initial state
    if(grn%n_solve < sys%n_state) call MessageText('Error','S:RunGroundState:: The number of state solved is smaller than the number of state occupied.')
    allocate(st%psi(gr%NrTotal, sys%n_state, gr%NkTotal))
    st%psi = dcmplx(0.d0, 0.d0)


    ! Solve the eigenvalue problem

    !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(IsThisCpuWait, lst, lfi, l, ll, k_sub, i, j, IndexS, IndexM, i_dim, eigenstate)
    call OpenMPIndex(gr%NkTotal, IsThisCpuWait, lst, lfi, 'Conv1', 'Report')

    do l = lst, lfi

      ! Progress printing
      if(OMP_GET_THREAD_NUM() == 0) then
        if(mod(l, progress_interval) == 1) print*, 'Ground state solving; current, chunk, and total k points:', l, lfi-lst, gr%NkTotal
      end if 

      ! Specify specific crystal momentum for the sub-Hamiltonian
      IndexS = l
      call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
      ll=IndexM

      do i_dim = 1, gr%dim
        k_sub(i_dim) = gr%k(i_dim)%xyz(ll(i_dim))  
      end do

      ! Solve the sub-eigenvalue problem and assign the initial states
      A = 0.d0
      call SolveSubHamiltonian(ExPo, gr, grn%SolMethod, grn%n_solve, A, k_sub, grn%ev(:, l), eigenstate)
      st%psi(:, :, l) = eigenstate(:, 1:sys%n_state)

      ! Form the pragmatic wavefunction by multiplying the weight and dr for space integration(so we don't need to multiply them when calculating integration in space).
      ! However, ZHEEV has already done this for us. So we do nothing. This part will be tricky when non-uniform grid is considered.
      ! Note that weight and dk in k space is not multiplied here.


    end do

    !$OMP END PARALLEL


    ! Plot the band and band difference structure
    ! FIXME: The way of data writing here should be refined in the future.
    select case (gr%dim)
    case (1)

      write(WU_ev, *) '#1D Band Structure'
      write(WU_ev, *) '#kx,band energy(eV), band energy difference from upper band(eV)'

      do i = 1, grn%n_solve 

        write(WU_ev, *) '#Band:', i

        do j =1, gr%Nk(1)
          if(i == grn%n_solve) then
            !write(WU_ev, '(2ES,1X,A)') gr%k(1)%xyz(j) / gr%RecLV(1), grn%ev(i, j)/u_eV, 'Highest solved band so no band difference.' ! k in recirpocal lattice vector length
            write(WU_ev, '(2ES,1X,A)') gr%k(1)%xyz(j), grn%ev(i, j)/u_eV, 'Highest solved band so no band difference.' ! k in au 
          else
            !write(WU_ev, '(3ES)') gr%k(1)%xyz(j) / (gr%RecLV(1)), grn%ev(i, j)/u_eV, (grn%ev(i+1, j)-grn%ev(i, j))/u_eV ! k in recirpocal lattice vector length
            write(WU_ev, '(3ES)') gr%k(1)%xyz(j), grn%ev(i, j)/u_eV, (grn%ev(i+1, j)-grn%ev(i, j))/u_eV   ! k in au
          end if
        end do

        write(WU_ev, *) ''
        write(WU_ev, *) ''

      end do

    case (2)

      write(WU_ev, *) '#2D Band Structure'
      write(WU_ev, *) '#kx, ky, band energy(eV), band energy difference from upper band(eV)'

      do i = 1, grn%n_solve 

        write(WU_ev, *) '#Band:', i

        do j =1, gr%NkTotal

          IndexS = j
          call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)

          if(i == grn%n_solve) then
            write(WU_ev, '(3ES,1X,A)') gr%k(1)%xyz(IndexM(1)) / gr%RecLV(1), gr%k(2)%xyz(IndexM(2)) / gr%RecLV(2), grn%ev(i, j)/u_eV, 'Highest solved band so no band difference.'
          else
            write(WU_ev, '(4ES)') gr%k(1)%xyz(IndexM(1)) / gr%RecLV(1), gr%k(2)%xyz(IndexM(2)) / gr%RecLV(2), grn%ev(i, j)/u_eV, (grn%ev(i+1, j)-grn%ev(i, j))/u_eV
          end if

          if(mod(j, gr%Nk(1)) == 0) write(WU_ev, *) ''

        end do

        write(WU_ev, *) ''

      end do

    case default

      call MessageText('Warning','S:RunGroundState:: Currently plotting band structure in dimension other than 1 or 2 is not supported.')

    end select 


    ! Calculate the cutted band structure
    call Parse('BandCutN', grn%N_BandCut)

    allocate(grn%k_BandCutStart(gr%dim))
    allocate(grn%k_BandCutEnd(gr%dim))
    call Parse('BandCutStartPoint', gr%dim, grn%k_BandCutStart)
    call Parse('BandCutEndPoint', gr%dim, grn%k_BandCutEnd)

    allocate(BandCutStructure(grn%N_BandCut,grn%n_solve))
    allocate(BandCutK(grn%N_BandCut))

    BandCutDk = 0.d0
    do i_dim = 1, gr%dim
      BandCutDk(i_dim) = (grn%k_BandCutEnd(i_dim) - grn%k_BandCutStart(i_dim))/dble(grn%N_BandCut - 1)
    end do

    do l = 1, grn%N_BandCut
      BandCutKtmp = (grn%k_BandCutStart + BandCutDk*dble(l - 1))*gr%RecLV
      BandCutK(l) = dsqrt(sum((BandCutKtmp - grn%k_BandCutStart*gr%RecLV)**2.d0))
      A = 0.d0
      call SolveSubHamiltonian(ExPo, gr, grn%SolMethod, grn%n_solve, A, BandCutKtmp, BandCutStructure(l, :), eigenstate)
    end do 


    ! Plot the cut band structure
    write(WU_BandCut, *) '# Band Cut Structure'
    write(WU_BandCut, *) '# k start:', grn%k_BandCutStart
    write(WU_BandCut, *) '# k end:', grn%k_BandCutEnd
    write(WU_BandCut, '(A)') '# Displacement from start point along designated direction(au), band energy(eV), band energy difference(eV)'

    do i = 1, grn%n_solve 

      write(WU_BandCut, *) '#Band:', i

      do l =1, grn%N_BandCut
        if(i == grn%n_solve) then
          write(WU_BandCut, '(2ES,X,A)') BandCutK(l), BandCutStructure(l, i)/u_eV, 'Highest solved band so no band difference.'
        else
          write(WU_BandCut, '(3ES)') BandCutK(l), BandCutStructure(l, i)/u_eV, (BandCutStructure(l, i + 1) - BandCutStructure(l, i))/u_eV
        end if
      end do

      write(WU_BandCut, *) ''
      write(WU_BandCut, *) ''

    end do


    ! Calculate the band gap between lowest conduction band and highest valence band
    if(grn%n_solve < sys%n_state + 1) then

      call MessageText('Warning','S:RunGroundState:: Cannot calculate the band gap since the number of bands solved is not enough.')

    else

      l = minloc(grn%ev(sys%n_state + 1,:) - grn%ev(sys%n_state,:), DIM=1)

      IndexS = l
      call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nk)
      ll = IndexM 

      write(*, '(A,X,I,X,A,X,I)') 'Minimun band gap between band', sys%n_state, 'and', sys%n_state + 1
      write(*, '(A)') 'is located at crystal momentum:'
      do i_dim = 1, gr%dim
        write(*,'(I,X,A,X,ES)') i_dim, 'th dimension:', gr%k(i_dim)%xyz(ll(i_dim))
      end do

      write(*, '(A)') 'The band gap energy is:'
      write(*, '(ES,X,A)') (grn%ev(sys%n_state + 1, l) - grn%ev(sys%n_state, l))/u_eV, 'eV'
      write(*,*) ''

    end if


    ! Calculate the effective mass of the lowest conduction - highest valence band 
    !   at a specific point for each direction
    ! Note that the number of k point in each directions must be at least 5
    !FIXME: add error detection if the system is not periodic(cannot calculate effective mass)
    !FIXME: add error detection if the first conduction band(n_state+1) is not solved
    !FIXME: accuracy of 2nd derivative decrease a lot when the number of grid point is odd. Not knowing why.
    allocate(k_effective_mass(gr%dim))

    k_effective_mass = 0.d0 ! Assign the point here

    ! Determine the nearest grid point around the k point of interest.
    if (any(gr%Nk < 5)) then 

      call MessageText('Warning','S:RunGroundState:: Cannot calculate effective mass because the number of grid point < number of point in stencil.')

    else

      do i_dim = 1, gr%dim
        ll(i_dim) = minloc(abs(gr%k(i_dim)%xyz(:) - k_effective_mass(i_dim)), DIM=1)
        if ((ll(i_dim) + 2  > gr%Nk(i_dim)) .or. (ll(i_dim) - 2 < 1)) then 
          call MessageText('Error','S:RunGroundState:: The assigned point for calculating effective mass is too close to the boundary.') ! FIXME: this check is here because I didn't use periodicity.
        end if
      end do

      write(*, '(A)') 'Effective mass of lowest conduction - highest valence band:'
      do i_dim = 1, gr%dim

        allocate(coefD2(-2:2))
        call Stencil1D(5, 2, k_effective_mass(i_dim), gr%k(i_dim)%xyz(ll(i_dim)-2:ll(i_dim)+2), coefD2(-2:2))

        effective_mass = 0.d0
        do i_stencil = -2, 2
          jj = ll
          jj(i_dim) = ll(i_dim) + i_stencil
          IndexM = jj
          call IndConv('M2S', IndexM, IndexS, gr%dim, gr%Nk)
          j = IndexS
          effective_mass = effective_mass + coefD2(i_stencil)*(grn%ev(sys%n_state+1, j) - grn%ev(sys%n_state, j))
        end do

        effective_mass = 1.d0/effective_mass

        write(*, '(A,1X,I,1X,A,1X,ES,1X,A)') '  Dimension', i_dim, ':', effective_mass, 'au.'

        deallocate(coefD2)

      end do

    end if

    ! FIXME: this part is ugly. Remove it after the paper is finished.
    ! Plot for comparison between Kane's band and the actual band
    if(gr%dim == 1) then
      write(WU_KaneComparison, *) "# k, band difference(E-Ev, v=highest valence)(eV), Kane's band(eV)"
      i = sys%n_state + 1
      do j = 1, gr%Nk(1)
        k_sub(1) = gr%k(1)%xyz(j) - (i - sys%n_state - 1)*gr%RecLV(1)
        !write(WU_KaneComparison, '(3ES)') k_sub(1) / gr%RecLV(1), (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in reciprocal lattice vector
        write(WU_KaneComparison, '(3ES)') k_sub(1)              , (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in au
      end do
      write(WU_KaneComparison, *) ''
      i = sys%n_state + 2
      do j = gr%Nk(1)/2 + 1, gr%Nk(1)
        k_sub(1) = gr%k(1)%xyz(j) - (i - sys%n_state - 1)*gr%RecLV(1)
        !write(WU_KaneComparison, '(3ES)') k_sub(1) / gr%RecLV(1), (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in reciprocal lattice vector
        write(WU_KaneComparison, '(3ES)') k_sub(1)              , (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in au

      end do
      write(WU_KaneComparison, *) ''
      do j = 1, gr%Nk(1)/2
        k_sub(1) = gr%k(1)%xyz(j) + (i - sys%n_state - 1)*gr%RecLV(1)
        !write(WU_KaneComparison, '(3ES)') k_sub(1) / gr%RecLV(1), (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in reciprocal lattice vector
        write(WU_KaneComparison, '(3ES)') k_sub(1)              , (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in au
      end do
      write(WU_KaneComparison, *) ''
      i = sys%n_state + 3
      do j = 1, gr%Nk(1)/2
        k_sub(1) = gr%k(1)%xyz(j) - (i - sys%n_state - 2)*gr%RecLV(1)
        !write(WU_KaneComparison, '(3ES)') k_sub(1) / gr%RecLV(1), (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in reciprocal lattice vector
        write(WU_KaneComparison, '(3ES)') k_sub(1)              , (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in au
      end do
      write(WU_KaneComparison, *) ''
      do j = gr%Nk(1)/2 + 1, gr%Nk(1)
        k_sub(1) = gr%k(1)%xyz(j) + (i - sys%n_state - 2)*gr%RecLV(1)
        !write(WU_KaneComparison, '(3ES)') k_sub(1) / gr%RecLV(1), (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in reciprocal lattice vector
        write(WU_KaneComparison, '(3ES)') k_sub(1)              , (grn%ev(i, j) - grn%ev(sys%n_state,j))/u_eV, Kane(k_sub(1))/u_eV ! k in au
      end do
      write(WU_KaneComparison, *) ''
    end if

  contains

    function Kane(k)
      implicit none
      real(kind=dp) :: k, Kane
      
      Kane = 4.18d0*u_eV*sqrt(1.d0 + k*k/(4.18d0*u_eV*0.083))

    end function Kane


    ! Plot the density of initial state?? I should check the definition of density in a crystal.
!    j = gr%Nr(1) + 1
!    allocate(plot_r(j))
!    allocate(plot_RD(j))
!    plot_r(1:j-1) = gr%r(1)%xyz(:)
!    plot_r(j) = -(gr%r(1)%xyz(1) - gr%RShift(1)) + gr%RShift(1)
!    plot_RD = 0.d0
!    do i = 1, sys%n_state
!      do l = 1, gr%Nk(1)
!        plot_RD(1:j-1) = plot_RD(1:j-1) + abs(psi(:,l,i))**2.d0 
!      end do
!    end do
!    plot_RD(j) = plot_RD(1) 
!    call WriteDataCurve2D_DoubleReal(WU_ini, 'Initital State Density', j, plot_RD, plot_r, 1.d0, gr%BoxL(1), 0, j)

    
  end subroutine RunGroundState

  !---------------------------------------------------------------
  subroutine RunPotentialOptimization(gr, sys, ExPo, grn)
    use omp_lib
    implicit none
    type(grid_t), intent(in) :: gr
    type(system_t), intent(in) :: sys
    type(ExternalPotential_t), intent(inout) :: ExPo
    type(ground_t), intent(inout) :: grn

    ! Internal
    type(ExternalPotential_t) :: EP
    integer, parameter :: progress_interval = 10
    integer, parameter :: iteration_max = 300
    integer :: i, j, l, hst, hfi, cst, cfi, jjj, i_dim, i_stencil, i_Fourier, iteration
    integer :: IndexS
    integer, allocatable :: ii(:), jj(:), ll(:)
    integer, allocatable :: IndexM(:)
    real(kind=dp) :: tmp
    real(kind=dp) :: cost(-1:1)
    real(kind=dp) :: effective_mass
    real(kind=dp) :: A(gr%dim)
    real(kind=dp) :: k_sub(gr%dim)
    real(kind=dp) :: FourierStepSize = 0.525d0
    real(kind=dp) :: converge_tolerance = 5.d-2*u_eV
    real(kind=dp), allocatable :: coefD1(:)
    real(kind=dp), allocatable :: k_effective_mass(:)
    real(kind=dp), allocatable :: Band(:,:)
    real(kind=dp), allocatable :: BandInitial(:,:)
    real(kind=dp), allocatable :: TargetBand(:,:)
    real(kind=dp), allocatable :: FourierTmp(:,:)
    real(kind=dp), allocatable :: FourierChange(:,:)
    real(kind=dp), allocatable :: FourierGrad(:,:)
    complex(kind=dp), allocatable :: eigenstate(:,:)

    ! for OpenMP
    logical :: IsThisCpuWait
    integer :: lst, lfi

    ! Writer unit
    integer :: WU_BandOptimized, WU_cost, WU_target, WU_ext

    open(newunit = WU_BandOptimized, file = 'band_optimized.dat')
    open(newunit = WU_cost, file = 'cost.dat')

    ! Preparation for index conversion
    allocate(IndexM(gr%dim))
    allocate(ii(gr%dim))
    allocate(jj(gr%dim))
    allocate(ll(gr%dim))

    allocate(coefD1(3))

    call Parse('GroundSolveMethod', grn%SolMethod)
    call Parse('GroundSolveState', grn%n_solve)
    allocate(eigenstate(gr%NrTotal, grn%n_solve))

    allocate(Band(gr%Nk(1),grn%n_solve))
    allocate(BandInitial(gr%Nk(1),grn%n_solve))
    allocate(TargetBand(gr%Nk(1), grn%n_solve))
    call TargetData()

    call ExternalPotential(EP, gr)
    allocate(FourierChange(0:EP%FourierMax, gr%dim))
    allocate(FourierGrad(0:EP%FourierMax, gr%dim))
    allocate(FourierTmp(0:EP%FourierMax, gr%dim))
    FourierChange = 0.0002d0 ! Guessed initial change

    ! Calculate and setup initial band and cost
    iteration = 0
    k_sub(2) = 0.d0
    do l = 1, gr%Nk(1)
      k_sub(1) = gr%k(1)%xyz(l)
      A = 0.d0
      call SolveSubHamiltonian(EP, gr, grn%SolMethod, grn%n_solve, A, k_sub, BandInitial(l, :))
    end do 
    cost(0) = CostFunction()
    write(WU_cost, '(A)') '# Cost over Iterations'
    write(WU_cost, '(A)') '# Iteration, cost(eV)'
    print*, iteration, cost(0)/u_eV
    write(WU_cost, '(I,X,ES)') iteration, cost(0)/u_eV
    close(WU_cost)

    do iteration = 1, iteration_max
      ! Calculate gradient
      do i_dim = 1, gr%dim
        do i_Fourier = 0, EP%FourierMax
          ! forward cost
          FourierTmp = EP%Fourier
          FourierTmp(i_Fourier, i_dim) = EP%Fourier(i_Fourier, i_dim) + FourierChange(i_Fourier, i_dim)
          call UpdateStaticPotential(gr, EP%FourierMax, FourierTmp, EP%V0, EP%V_static) ! keep in mind after calling this EP%Fourier does not give EP%V_static.
          !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(IsThisCpuWait, lst, lfi, l, k_sub)
          call OpenMPIndex(gr%Nk(1), IsThisCpuWait, lst, lfi, 'Conv1', 'Report')
          k_sub(2) = 0.d0
!          do l = 1, gr%Nk(1)
          do l = lst, lfi
            k_sub(1) = gr%k(1)%xyz(l)
            A = 0.d0
            call SolveSubHamiltonian(EP, gr, grn%SolMethod, grn%n_solve, A, k_sub, Band(l, :))
          end do 
          !$OMP END PARALLEL
          cost(1) = CostFunction()
          ! backward cost
          FourierTmp = EP%Fourier
          FourierTmp(i_Fourier, i_dim) = EP%Fourier(i_Fourier, i_dim) - FourierChange(i_Fourier, i_dim)
          call UpdateStaticPotential(gr, EP%FourierMax, FourierTmp, EP%V0, EP%V_static) ! keep in mind after calling this EP%Fourier does not give EP%V_static.
          !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(IsThisCpuWait, lst, lfi, l, k_sub)
          call OpenMPIndex(gr%Nk(1), IsThisCpuWait, lst, lfi, 'Conv1', 'Report')
          k_sub(2) = 0.d0
!          do l = 1, gr%Nk(1)
          do l = lst, lfi
            k_sub(1) = gr%k(1)%xyz(l)
            A = 0.d0
            call SolveSubHamiltonian(EP, gr, grn%SolMethod, grn%n_solve, A, k_sub, Band(l, :))
          end do 
          !$OMP END PARALLEL
          cost(-1) = CostFunction()
          ! gradient
          FourierGrad(i_Fourier, i_dim) = (0.5d0*cost(1) - 0.5d0*cost(-1))/FourierChange(i_Fourier, i_dim)
          !write(*,'(2I,4ES)') i_dim, i_Fourier, cost(-1)/u_eV, cost(0)/u_eV, cost(1)/u_eV, FourierGrad(i_Fourier, i_dim)
        end do
      end do
 
      ! Change Fourier component according to the gradient, and calculate new cost.
      EP%Fourier = EP%Fourier - FourierGrad*FourierStepSize
      call UpdateStaticPotential(gr, EP%FourierMax, EP%Fourier, EP%V0, EP%V_static) ! this make EP%Fourier give EP%V_static again.
      k_sub(2) = 0.d0
      do l = 1, gr%Nk(1)
        k_sub(1) = gr%k(1)%xyz(l)
        A = 0.d0
        call SolveSubHamiltonian(EP, gr, grn%SolMethod, grn%n_solve, A, k_sub, Band(l, :))
      end do 
      cost(0) = CostFunction()
      print*, iteration, cost(0)/u_eV
      open(newunit = WU_cost, file='cost.dat', status='OLD', action='WRITE', position='APPEND')
      write(WU_cost, '(I,X,ES)') iteration, cost(0)/u_eV
      close(WU_cost)

      if(cost(0) <= converge_tolerance) then
        write(*,'(A)') 'Convergence in cost is atttained:' 
        write(*,'(A,X,ES)') 'Convergence criterion(eV):', converge_tolerance/u_eV 
        exit
      end if

    end do

    write(*,'(A,X,ES)') 'Final cost(eV):', cost(0)/u_eV 
    print*, 'Fourier components for'
    do i_dim = 1, gr%dim
      print*, 'dimension', i_dim, ':'
      print*, EP%Fourier(:,i_dim)
    end do

    ExPo%V_static = EP%V_static

    ! Plot the optimized potential
    open(newunit = WU_ext, file = 'V_ext_optimized.dat')
    !FIXME:this writing part should be adapted===============
    ! The way of data writing here should be refined in the future.
    select case (gr%dim)
    case (1)
      call WriteDataCurve2D_DoubleReal(WU_ext, 'External Potential', gr%Nr(1), ExPo%V_static(1:gr%Nr(1)), gr%r(1)%xyz, 1.d0, 1.d0, 0, gr%Nr(1))
    case (2)
      do i =1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', IndexM, IndexS, gr%dim, gr%Nr)
        write(WU_ext, '(3ES)') gr%r(1)%xyz(IndexM(1)), gr%r(2)%xyz(IndexM(2)), ExPo%V_static(i)  !direct writing
        if(mod(i,gr%Nr(1)) == 0) write(WU_ext, *) ''                                       !direct writing
      end do
    case default
      call MessageText('Warning','S:ExternalPotential:: Currently plotting external potential with dimension =3 is not supported.')
    end select 

    ! Plot the optimized and target band structures
    write(WU_BandOptimized, *) '# Optimized and target Band Structures'
    write(WU_BandOptimized, '(A)') '# kx/g, band energy(eV), band energy difference(eV),&
                                          & target band energy, target band energty difference,&
                                          & initial band energy, initial band energy difference'
    do i = 1, grn%n_solve 
      write(WU_BandOptimized, *) '#Band:', i
      do l =1, gr%Nk(1)
        if(i == grn%n_solve) then
          write(WU_BandOptimized, '(2ES,X,A,X,ES,X,A,X,ES,X,A)') gr%k(1)%xyz(l)/gr%RecLV(1), &
                                                                &Band(l, i)/u_eV, 'Unavailable', &
                                                                &TargetBand(l, i)/u_eV, 'Unavailable', &
                                                                &BandInitial(l, i)/u_eV, 'Unavailable'
        else
          write(WU_BandOptimized, '(7ES)') gr%k(1)%xyz(l)/gr%RecLV(1), &
                                          &Band(l, i)/u_eV, (Band(l, i + 1) - Band(l, i))/u_eV, &
                                          &TargetBand(l, i)/u_eV, (TargetBand(l, i + 1) - TargetBand(l, i))/u_eV, &
                                          &BandInitial(l, i)/u_eV, (BandInitial(l, i + 1) - BandInitial(l, i))/u_eV
                                          
        end if
      end do
      write(WU_BandOptimized, *) ''
      write(WU_BandOptimized, *) ''
    end do

    contains

      subroutine TargetData()
        implicit none
        real(kind=dp) :: dummy1, dummy2
        open(newunit = WU_target, file = 'BandOptimizationTarget.rec')
        do j = 1, grn%n_solve ! Note that grn%n_solve should not be too large to read the words for highest band difference.
          do i = 1, gr%Nk(1)
            read(WU_target, *) dummy1, TargetBand(i, j), dummy2
          end do
        end do
        TargetBand = TargetBand*u_eV
      end subroutine TargetData

      Function CostFunction()
        implicit none
        integer :: TargetBandMatchStart = 2
        integer :: BandMatchStart = 1
        integer :: BandMatchChunk = 2
        integer :: TargetBandMatchEnd
        integer :: BandMatchEnd
        real(kind=dp) :: CostFunction
        integer :: i_band, i_k

        TargetBandMatchEnd = TargetBandMatchStart + BandMatchChunk
        BandMatchEnd = BandMatchStart + BandMatchChunk
        
        CostFunction = 0.d0
        do i_band = 1, BandMatchChunk
          do i_k = 1, gr%Nk(1)
            CostFunction = CostFunction + abs( (TargetBand(i_k, TargetBandMatchStart + i_band) - TargetBand(i_k, TargetBandMatchStart)) - &
                                             & (      Band(i_k, BandMatchStart       + i_band) -       Band(i_k, BandMatchStart      ))     )
          end do
        end do
        CostFunction = CostFunction/dble(gr%Nk(1)*BandMatchChunk)

      end function CostFunction

  end subroutine RunPotentialOptimization


  !---------------------------------------------------------------
  subroutine UpdateStaticPotential(gr, FourierMax, Fourier, V0, V_static)
    implicit none
    type(grid_t), intent(in) :: gr
    integer, intent(in) :: FourierMax
    real(kind=dp), intent(in) :: Fourier(0:FourierMax, gr%dim)
    real(kind=dp), intent(in) :: V0(gr%dim)
    real(kind=dp), intent(out) :: V_static(gr%NrTotal)

    ! Internal
    integer :: i, j, i_dim, IndexS
    integer :: IndexM(gr%dim)
    real(kind=dp) :: tmp

    V_static = 1.d0
    do i = 1, gr%NrTotal
      IndexS = i
      call IndConv('S2M', IndexM, IndexS, gr%Dim, gr%Nr)
      do i_dim = 1, gr%Dim
        tmp = Fourier(0, i_dim)
        do j = 1, FourierMax
          tmp = tmp + Fourier(j, i_dim)*2.d0*cos( dble(j)*gr%RecLV(i_dim)*gr%r(i_dim)%xyz(IndexM(i_dim)) )
        end do 
        V_static(i) = V_static(i)*tmp
      end do
      V_static(i) = V_static(i)*V0(1)
    end do

  end subroutine UpdateStaticPotential

  !---------------------------------------------------------------
  subroutine SolveSubHamiltonian(ExPo, gr, method, NumberOfSolvedState, A, k_sub, eigenvalue, eigenstate)
    implicit none
    type(ExternalPotential_t) :: ExPo
    type(grid_t), intent(in) :: gr
    integer, intent(in) :: method
    integer, intent(in) :: NumberOfSolvedState
    real(kind=dp), intent(in) :: k_sub(gr%dim)
    real(kind=dp), intent(in) :: A(gr%dim)
    real(kind=dp), intent(inout) :: eigenvalue(NumberOfSolvedState)
    complex(kind=dp), intent(out), optional :: eigenstate(gr%NrTotal, NumberOfSolvedState)

    ! Internal
    logical :: HasContribution
    integer :: i, j, i_dim, i_stencil, i_delta
    integer :: IndexS
    integer, allocatable :: IndexM(:)
    integer, allocatable :: ii(:), jj(:)
    real(kind=dp), allocatable :: EValue(:)
    complex(kind=dp), allocatable :: h0(:,:)

    ! for ZHEEV 
    integer :: lwork
    integer :: info
    real(kind=dp), allocatable :: rwork(:)
    complex(kind=dp), allocatable :: work(:)

    select case(method)
    case (1) ! Solve by ZHEEV

      ! Index-conversion related setup
      allocate(ii(gr%dim))
      allocate(jj(gr%dim))
      allocate(IndexM(gr%dim))

      ! Allocate and set up initial value of arrays.
      lwork = 2*gr%NrTotal
      allocate(work(lwork))
      allocate(rwork(3*gr%NrTotal-2))
      allocate(EValue(gr%NrTotal))
      allocate(h0(gr%NrTotal, gr%NrTotal))
      h0 = dcmplx(0.d0, 0.d0)
 
      ! Build the Hamiltonian matrix
      do i = 1, gr%NrTotal
        IndexS = i
        call IndConv('S2M', ii, IndexS, gr%dim, gr%Nr)
        ! Lattice potential part
        h0(i, i) = h0(i, i) + dcmplx(ExPo%V_static(i), 0.d0) 
        ! [0.5*(|k|^2+|A|^2)] part and [k dot A] part
        do i_dim = 1, gr%dim
          h0(i, i) = h0(i, i) + dcmplx(0.5d0*(k_sub(i_dim)**2.d0 + A(i_dim)**2.d0) + k_sub(i_dim)*A(i_dim), 0.d0)
        end do
        do j = 1, gr%NrTotal
          IndexS = j
          call IndConv('S2M', jj, IndexS, gr%dim, gr%Nr)
          ! To determine if the element of Hamiltonian has contribution from 1st and 2nd derivative, and specify the correct stencil index for a chosen i,j.
          do i_dim = 1, gr%dim
            HasContribution = .false.
            if( abs(jj(i_dim)-ii(i_dim)) <= gr%n_SS) then
              HasContribution = .true.
              i_stencil = jj(i_dim) - ii(i_dim)
            elseif((jj(i_dim) - ii(i_dim)) >= (gr%Nr(i_dim) - gr%n_SS)) then
              HasContribution = .true.
              i_stencil = jj(i_dim) - ii(i_dim) - gr%Nr(i_dim)
            elseif((jj(i_dim) - ii(i_dim)) <= -(gr%Nr(i_dim) - gr%n_SS)) then
              HasContribution = .true.
              i_stencil = jj(i_dim) - ii(i_dim) + gr%Nr(i_dim)
            end if
            ! This part is just Kroneckers Delta function for directions other than i_dim
            if (gr%dim > 1) then
              do i_delta = 0, gr%dim - 2, 1
                if(ii(NextPeriodicNumber(i_dim + i_delta, gr%dim)) .ne. jj(NextPeriodicNumber(i_dim + i_delta, gr%dim))) HasContribution = .false.
              end do
            end if
            ! Insert the first and second derivatives parts -0.5*Laplacian -i(k+A) dot gradiant
            if(HasContribution) then
              h0(i, j) = h0(i, j) + dcmplx(-0.5d0*gr%D2(i_stencil, i_dim), 0.d0) &
                                 &+ dcmplx(0.d0, -(k_sub(i_dim) + A(i_dim))*gr%D1(i_stencil, i_dim))
            end if
          end do
        end do
      end do
 
      ! Solving the eigenvalue problem by ZHEEV and pass to output
      if(present(eigenstate)) then
        call ZHEEV('V', 'U', gr%NrTotal, h0, gr%NrTotal, EValue, work, lwork, rwork, info)
        eigenstate = h0(:, 1:NumberOfSolvedState)
      else
        call ZHEEV('N', 'U', gr%NrTotal, h0, gr%NrTotal, EValue, work, lwork, rwork, info)
      end if
      eigenvalue = EValue(1:NumberOfSolvedState)
      
    case default
      call MessageText('Error','S:SolveSubHamiltonian:: Currently method for solving eigenvalue problem only supports 1.')
    end select 

  end subroutine SolveSubHamiltonian

  !---------------------------------------------------------------
  subroutine RunTimeDependent(ExPo, gr, obsv, st, sys, td)
    implicit none
    type(ExternalPotential_t), intent(in)    :: ExPo
    type(grid_t),              intent(in)    :: gr
    type(observable_t),        intent(inout) :: obsv
    type(state_t),             intent(inout) :: st
    type(system_t),            intent(inout) :: sys
    type(td_t),                intent(inout) :: td

    ! Internal
    integer, parameter :: progress_print_interval = 1000
    integer, parameter :: field_osc_dir = 1
    integer :: t, t_eva, t_plot
    real(kind=dp) :: ShiftFieldAmplitudeRelativeFactor
    real(kind=dp) :: color_parameters(1, -100:100)
    real(kind=dp) :: tp(50), wp(50)
    real(kind=dp), allocatable :: coefD1(:)
    real(kind=dp), allocatable :: coefD2(:)
    real(kind=dp), allocatable :: normalization(:)
    real(kind=dp), allocatable :: energy(:)
    real(kind=dp), allocatable :: field_shift(:)
    real(kind=dp), allocatable :: field_vec_shift(:,:)
    real(kind=dp), allocatable :: field_vec_mid_shift(:,:)

    ! WU
    integer :: WU_field, WU_td, WU_BandProjection, WU_BandPopulationDistribution

    ! timing
    real(kind=dp) :: start

    open(newunit = WU_field, file = 'field.dat')

    call Parse('TimePropagationMethod', td%PropMethod)
    call Parse('Nt', td%Nt)
    if(td%Nt <= 1) call MessageText('Error','S:RunTimeDependent:: The number of time grid must be > 1.')
    call Parse('TD_ExpOrder', td%ExpOrder)
    call Parse('TD_PropagationTime', td%T_Pro)
    td%T_Pro = td%T_Pro*u_FemtoSecond
    call Parse('TD_PulseTime', td%T_Pul)
    td%T_Pul = td%T_Pul*u_FemtoSecond
    call Parse('TD_PulseCenter', td%T_PulCen)
    td%T_PulCen = td%T_PulCen*u_FemtoSecond

    call Parse('EvaluationInterval', obsv%EvaluationInterval)
    if((obsv%EvaluationInterval <= 0).or.(obsv%EvaluationInterval > td%Nt)) then
      call MessageText('Error','S:RunTimeDependent:: The interval in time grid for observation should be >= 1 and <= total number of time grid.')
    end if
    obsv%NtEvaluation = (td%Nt - 1)/obsv%EvaluationInterval + 1 ! this -1 and +1 comes from the fact that we evaluate observables at t=1, 1+1xInterval, 1+2xInterval. ...
    if(mod(td%Nt - 1, obsv%EvaluationInterval) .ne. 0) obsv%NtEvaluation = obsv%NtEvaluation + 1 ! this +1 comes from the fact that we always evaluate observable at the last time grid.
    allocate(obsv%EvGr2TmGr(obsv%NtEvaluation))
    do t_eva = 1, obsv%NtEvaluation
      obsv%EvGr2TmGr(t_eva) = (t_eva - 1)*obsv%EvaluationInterval + 1
      if(t_eva == obsv%NtEvaluation) obsv%EvGr2TmGr(t_eva) = td%Nt
    end do

    allocate(td%E0(gr%dim))
    call Parse('E0', gr%dim, td%E0)
    td%E0 = td%E0*u_VoltOverAngstrom 
    call Parse('Wavelength', td%lambda)
    td%lambda = td%lambda*u_NanoMeter

    td%dt = td%T_Pro/dble(td%nt-1)
    td%period = td%lambda/u_LightSpeed
    td%omega = 2.d0*pi/td%period

    allocate(td%time(td%nt))
    do t = 1, td%nt
      td%time(t) = dble(t-1)
    end do
    td%time = td%time*td%dt

    ! Write information
    write(*,'(A,1X,F,1X,A)'), 'Total propagtion time', td%t_Pro/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A,1X,I)'), 'Time step size:',td%dt/u_FemtoSecond, 'fs; Number of time grid:', td%Nt
    write(*,'(A,1X,F,1X,A)'), 'Pulse length:',td%T_Pul/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A)'), 'Laser wavelength:',td%lambda/u_NanoMeter, 'Nm.'
    write(*,'(A,1X,F,1X,A)'), 'Laser period:',td%period/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A)'), 'Laser photon energy:',td%omega/u_eV, 'eV.'
    write(*,'(A,1X,F,1X,A)'), 'Laser amplitude in x direction:',td%E0(1)/u_VoltOverAngstrom, 'V/Angstrom'
    write(*,'(A,1X,F,1X,A)'), 'The highest energy in the spectrum:', 2.d0*pi/td%dt/u_eV, 'eV.'

    ! Prepare the laser field
    call Parse('LaserChoice', td%LaserChoice)
    if(td%LaserChoice == 1) then ! Cos^4 envelope
      ! e-field
      allocate(td%field(td%nt))
      color_parameters(1,0) = 5.d0
      color_parameters(1,1) = ConversionIntensityToElectricField('B',td%E0(field_osc_dir),1.d0)*u_TW_Over_CM2
      color_parameters(1,2) = td%lambda
      color_parameters(1,3) = -0.5d0*pi
      color_parameters(1,4) = 0.d0
      color_parameters(1,-1) = td%T_PulCen
      color_parameters(1,-2) = td%T_Pul
      do t = 1, td%nt
        td%field(t) = LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t))
      enddo
      ! A-field
      allocate(field_vec(td%nt, gr%dim))
      allocate(field_vec_mid(td%nt, gr%dim))
      color_parameters(1,0) = 5.d0
      color_parameters(1,1) = ConversionIntensityToElectricField('B',td%E0(field_osc_dir),1.d0)*u_TW_Over_CM2
      color_parameters(1,2) = td%lambda
      color_parameters(1,3) = 0.d0*pi
      color_parameters(1,4) = 0.d0
      color_parameters(1,-1) = td%T_PulCen
      color_parameters(1,-2) = td%T_Pul
      field_vec = 0.d0
      field_vec_mid = 0.d0
      do t = 1, td%nt
        field_vec(t, field_osc_dir) = 1.d0/td%omega*LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t))
        ! Note that the field_vec_mid(t) is the vector potential at time time(t)+0.5dt. This is so for the time propagation.
        field_vec_mid(t, field_osc_dir) = 1.d0/td%omega*LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t)+0.5d0*td%dt)
      enddo
    elseif(td%LaserChoice == 2) then ! Cos^4 + shift envelope
      call Parse('ShiftFieldAmplitudeRelativeFactor', ShiftFieldAmplitudeRelativeFactor)
      ShiftFieldAmplitudeRelativeFactor = ShiftFieldAmplitudeRelativeFactor/100.d0
      ! e-field; main
      allocate(td%field(td%nt))
      color_parameters(1,0) = 5.d0
      color_parameters(1,1) = ConversionIntensityToElectricField('B',td%E0(field_osc_dir),1.d0)*u_TW_Over_CM2
      color_parameters(1,2) = td%lambda
      color_parameters(1,3) = -0.5d0*pi
      color_parameters(1,4) = 0.d0
      color_parameters(1,-1) = td%T_PulCen
      color_parameters(1,-2) = td%T_Pul
      do t = 1, td%nt
        td%field(t) = LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t))
      enddo
      ! e-field; shift
      allocate(field_shift(td%nt))
      tp(1) = 0.d0
      tp(2) = 0.5d0*td%T_Pul
      tp(3) = 0.5d0*td%T_Pul
      tp(4) = td%T_Pul
      do t = 1, td%nt
        field_shift(t) = ShiftFieldAmplitudeRelativeFactor*td%E0(field_osc_dir)*WindowGeneratorFunction(1, td%time(t), tp, wp)
      enddo
      td%field = td%field + field_shift
      ! A-field; main
      allocate(field_vec(td%nt, gr%dim))
      allocate(field_vec_mid(td%nt, gr%dim))
      color_parameters(1,0) = 5.d0
      color_parameters(1,1) = ConversionIntensityToElectricField('B',td%E0(field_osc_dir),1.d0)*u_TW_Over_CM2
      color_parameters(1,2) = td%lambda
      color_parameters(1,3) = 0.d0*pi
      color_parameters(1,4) = 0.d0
      color_parameters(1,-1) = td%T_PulCen
      color_parameters(1,-2) = td%T_Pul
      field_vec = 0.d0
      field_vec_mid = 0.d0
      do t = 1, td%nt
        field_vec(t, field_osc_dir) = 1.d0/td%omega*LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t))
        field_vec_mid(t, field_osc_dir) = 1.d0/td%omega*LaserFieldGeneratorFunction(1, Color_Parameters, td%time(t)+0.5d0*td%dt)
      enddo
      ! A-field; shift
      allocate(field_vec_shift(td%nt, gr%dim))
      allocate(field_vec_mid_shift(td%nt, gr%dim))
      tp(1) = 0.d0
      tp(2) = td%T_Pul
      tp(3) = 2.d0*td%T_Pul
      tp(4) = 3.d0*td%T_Pul
      field_vec_shift = 0.d0
      field_vec_mid_shift = 0.d0
      do t = 1, td%nt
        field_vec_shift(t, field_osc_dir) = ShiftFieldAmplitudeRelativeFactor*(-2.d0*td%E0(field_osc_dir)*(td%T_Pul)/pi)*WindowGeneratorFunction(6, td%time(t), tp, wp)
        field_vec_mid_shift(t, field_osc_dir) = ShiftFieldAmplitudeRelativeFactor*(-2.d0*td%E0(field_osc_dir)*(td%T_Pul)/pi)*WindowGeneratorFunction(6, td%time(t) + 0.5d0*td%dt, tp, wp)
      enddo
      field_vec = field_vec + field_vec_shift
      field_vec_mid = field_vec_mid + field_vec_mid_shift
    end if
    call WriteDataCurve2D_DoubleReal(WU_field, 'E-field', obsv%NtEvaluation, td%field, td%time, 1.d0, td%period, 0, td%nt)
    call WriteDataCurve2D_DoubleReal(WU_field, 'VectorPotential', obsv%NtEvaluation, field_vec(:,field_osc_dir), td%time, 1.d0, td%period, 0, td%nt)
    !call WriteDataCurve2D_DoubleReal(WU_field, 'VectorPotential', obsv%NtEvaluation, field_vec_mid(:,field_osc_dir), td%time+0.5d0*td%dt, 1.d0, td%period, 0, td%nt)

    ! Allocate and evaluate physical quantities
    allocate(obsv%norm(obsv%NtEvaluation))
    allocate(obsv%energy(obsv%NtEvaluation))
    allocate(obsv%MechanicalMomentum(obsv%NtEvaluation, gr%dim))

    call Parse('NumberOfBandForProjection', obsv%NumberOfBandForProjection)
    call Parse('SolveMethodForBandProjection', obsv%SolveMethodForBandProjection)
    allocate(obsv%Occupation(obsv%NumberOfBandForProjection, gr%NkTotal))

    ! Preparation for specific time-propagation method.
    select case(td%PropMethod)
      case (0)
      case default
        call MessageText('Error','S:RunTimeDependent:: Invalid choiec for time propagation method. Currently only propagation with direct application is allowed.')
    end select

    ! Time propagation
    t_eva = 1
    do t = 1, td%nt 

      ! Observable evaluations
      if((mod(t-1, obsv%EvaluationInterval) == 0) .or. (t == td%nt)) then
        call CalculateAndWriteObservable(t_eva)
        t_eva = t_eva + 1
        if(t == td%nt) exit ! the last time propagation is not needed as it makes state to time nt+1.
      end if

      ! Progress inidcator
      if(mod(t, progress_print_interval) == 1) print*, t, '/', td%Nt, '; Recent norm deviation:', abs(obsv%norm(t_eva - 1) - 1.d0)

      select case(td%PropMethod)
        case (0)
          call PropagationByDirectApplication()
        case (1)
          !call PropagationByDenseMatrix()
        case default
        call MessageText('Error','S:RunTimeDependent:: Invalid choice for time propagation method.')
      end select

    end do

  contains

    subroutine CalculateAndWriteObservable(t_eva_present)
      implicit none
      integer, intent(in) :: t_eva_present

      ! writing titles
      if(t_eva_present == 1) then
        open(newunit = WU_td, file = 'td.dat', status = 'replace')
        write(WU_td, '(A)') '# Time(OC), Normalization, Mechanical Momentum(au), Energy(eV)'
        close(WU_td)

        open(newunit = WU_BandProjection, file = 'band_projection.dat', status = 'replace')
        write(WU_BandProjection, '(23A)') '# Time(OC), &
                                        &# of non-Excited e, &
                                        &# of Excited e,&
                                        &# of e in band 1, &
                                        &# of e in band 2, &
                                        &# of e in band 3, &
                                        &# of e in band 4, &
                                        &# of e in band 5, &
                                        &# of e in band 6, &
                                        &# of e in band 7, &
                                        &# of e in band 8, &
                                        &# of e in band 9, &
                                        &# of e in band 10, &
                                        &# of e in band 11, &
                                        &# of e in band 12, &
                                        &# of e in band 13, &
                                        &# of e in band 14, &
                                        &# of e in band 15, &
                                        &# of e in band 16, &
                                        &# of e in band 17, &
                                        &# of e in band 18, &
                                        &# of e in band 19, &
                                        &# of e in band 20'
        close(WU_BandProjection)

        open(newunit = WU_BandPopulationDistribution, file = 'band_distribution.dat', status = 'replace')
        write(WU_BandPopulationDistribution, '(A)') '# Crystal Momentum(au), Energy(eV), Number of Electrons per Cell at this K an E'
        close(WU_BandPopulationDistribution)
      end if

      ! performing calculations  
      call CheckNormalization(gr, sys, st%psi, obsv%norm(t_eva_present))
      call EvaluateMechanicalMomentum(gr, sys, st%psi, field_vec(obsv%EvGr2TmGr(t_eva_present),:), obsv%MechanicalMomentum(t_eva,:))
      call EvaluateEnergy(ExPo, gr, sys, st%psi, field_vec(obsv%EvGr2TmGr(t_eva_present),:), obsv%Energy(t_eva))
      !if((t>=20000).and.(t<=60000)) then
!        call CalculateBandProjection(ExPo, gr, st, sys, obsv%NumberOfBandForProjection, 1, field_vec(obsv%EvGr2TmGr(t_eva_present),:), obsv%Occupation)
      !end if
      !if(t>60000) stop

      ! writing data
      open(newunit = WU_td, file = 'td.dat', access = 'append', status = 'old')
      write(WU_td,'(4ES)') td%time(obsv%EvGr2TmGr(t_eva_present))/td%period, obsv%norm(t_eva), obsv%MechanicalMomentum(t_eva,field_osc_dir), obsv%Energy(t_eva)/u_eV
      close(WU_td)

      open(newunit = WU_BandProjection, file = 'band_projection.dat', access = 'append', status = 'old')
      write(WU_BandProjection, '(23ES)') td%time(obsv%EvGr2TmGr(t_eva_present))/td%period,&
                                      & sum(obsv%Occupation(1:sys%n_state,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(sys%n_state+1:obsv%NumberOfBandForProjection,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(1,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(2,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(3,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(4,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(5,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(6,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(7,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(8,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(9,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(10,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(11,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(12,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(13,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(14,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(15,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(16,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(17,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(18,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(19,:))/gr%NkTotal,&
                                      & sum(obsv%Occupation(20,:))/gr%NkTotal

      close(WU_BandProjection)

    end subroutine


    subroutine PropagationByDirectApplication()
      use omp_lib
      implicit none
      real(kind=dp), allocatable :: A(:)
      complex(kind=dp) :: TaylorFactor
      complex(kind=dp) :: csum
 
      ! Internal
      integer :: i_state, i_ExpOrder
      integer :: l, IndexS
      integer, allocatable :: ll(:), IndexM(:)
      complex(kind=dp), allocatable :: psi_tmp1(:,:,:), psi_tmp2(:,:,:)
 
      ! timing
      real(kind=dp) :: start, ending
      integer :: c, crate, cmax
 
      allocate(A(gr%dim))
      A = field_vec_mid(t, :)
      allocate(ll(gr%dim))
      allocate(IndexM(gr%dim))
      allocate(psi_tmp1(gr%NrTotal, sys%n_state, gr%NkTotal))
      allocate(psi_tmp2(gr%NrTotal, sys%n_state, gr%NkTotal))
 
      psi_tmp1 = st%psi
      TaylorFactor = dcmplx(1.d0,0.d0)
      do i_ExpOrder = 1, td%ExpOrder
        call OperatorToBlochWaveAndRemoveExpPart(gr, sys, 'Hamiltonian', A, psi_tmp1, psi_tmp2, ExPo)
        TaylorFactor = TaylorFactor*dcmplx(0.d0, -td%dt/dble(i_ExpOrder))
        st%psi = st%psi + TaylorFactor*psi_tmp2
        psi_tmp1 = psi_tmp2
      end do
 
    end subroutine

  end subroutine RunTimeDependent

  !---------------------------------------------------------------
  subroutine HHG_Spectrum(gr, obsv, td, VelocityOrAccelerationForm, direction, anls)
  implicit none
  type(grid_t),       intent(in) :: gr
  type(observable_t), intent(in) :: obsv
  type(td_t),         intent(in) :: td
  character,          intent(in) :: VelocityOrAccelerationForm
  integer,            intent(in) :: direction ! FIXME: add error detection for direction
  type(analysis_t), intent(out) :: anls

  ! Internal
  real(kind=dp) :: dw
  real(kind=dp) :: tp(50), wp(50)
  real(kind=dp) :: tmp(2)
  real(kind=dp), allocatable :: hhg(:, :), hhg_f(:)
  real(kind=dp), allocatable :: window(:)
  complex(kind=dp) :: csum
  integer :: i, i_dir, t, t_eva
  integer :: WU_hhg

  open(newunit = WU_hhg, file = 'hhg.dat')

  call Parse('N_HHG', anls%N_HHG)
  call Parse('HHG_EnergyRange', 2, tmp)
  anls%HHG_EL = tmp(1)*u_eV
  anls%HHG_EU = tmp(2)*u_eV
  dw = (anls%HHG_EU - anls%HHG_EL)/dble(anls%N_HHG - 1)
  allocate(hhg_f(anls%N_HHG))
  do i = 1, anls%N_HHG
    hhg_f(i) = dble(i-1)*dw + anls%HHG_EL
  end do

  allocate(window(td%Nt))
  window = 0.d0
  tp(1) = td%T_PulCen
  wp(1) = td%T_Pul
  call WindowGenerator(td%Nt-1, 5, td%time(1:td%Nt), tp, window(:), wp)

  allocate(hhg(anls%N_HHG, gr%dim))

  do i_dir = direction, direction ! just designated direction
!  do i_dir = 1, gr%dim ! calculate all direction
    do i = 1, anls%N_HHG
      csum = dcmplx(0.d0,0.d0)
      do t_eva = 1, obsv%NtEvaluation
        if(t_eva == 1) then
          csum = csum + dcmplx(window(obsv%EvGr2TmGr(t_eva))*obsv%MechanicalMomentum(t_eva, i_dir)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva + 1)) - td%Time(obsv%EvGr2TmGr(t_eva)    )), 0.d0)*&
                       &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
        elseif(t_eva == obsv%NtEvaluation) then
          csum = csum + dcmplx(window(obsv%EvGr2TmGr(t_eva))*obsv%MechanicalMomentum(t_eva, i_dir)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva))     - td%Time(obsv%EvGr2TmGr(t_eva - 1))), 0.d0)*&
                       &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
        else
          csum = csum + dcmplx(window(obsv%EvGr2TmGr(t_eva))*obsv%MechanicalMomentum(t_eva, i_dir)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva + 1)) - td%Time(obsv%EvGr2TmGr(t_eva - 1))), 0.d0)*&
                       &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
        end if
      end do
      if(VelocityOrAccelerationForm == 'V') then
        hhg(i, :) = 2.d0*dlog10(cdabs(csum))                       
      else
        hhg(i, i_dir) = 2.d0*dlog10(cdabs(csum*dcmplx(0.d0, hhg_f(i)))) 
      end if
    enddo
  end do

  write(WU_hhg, *) 'HHG Spectrum' 
  write(WU_hhg, *) 'Frequency(eV), Frequency(Order), Intensity(log scale)' 
  do i = 1, anls%n_hhg
    write(WU_hhg,'(3ES)') hhg_f(i)/u_eV, hhg_f(i)/td%omega, hhg(i, direction) 
  end do
  write(WU_hhg,*) ''
  write(WU_hhg,*) ''

!  call WriteDataCurve2D_DoubleReal(WU_hhg, 'HHG Spectrum(eV)', sys%n_hhg, hhg, hhg_f, 1.d0, u_eV, 0, sys%n_hhg)
!  call WriteDataCurve2D_DoubleReal(WU_hhg, 'HHG Spectrum(order)', sys%n_hhg, hhg, hhg_f, 1.d0, sys%td%omega, 0, sys%n_hhg)

  end subroutine HHG_Spectrum

  !---------------------------------------------------------------
  subroutine IndConv(Dir, IndexM, IndexS, MaxDim, NforDim, ZeroOrOneConv)
    implicit none
    character(len = 3), intent(in) :: Dir
    character(len = *), optional, intent(in) :: ZeroOrOneConv
    integer, intent(in) :: MaxDim
    integer, intent(in) :: NforDim(MaxDim)
    integer, intent(inout) :: IndexM(MaxDim)
    integer, intent(inout) :: IndexS
    integer :: beg
    integer :: ii
  
    beg = 1
    if(present(ZeroOrOneConv) .and. (ZeroOrOneConv == 'Zero' .or. ZeroOrOneConv == 'zero')) beg = 0

    select case (trim(Dir))
    case ('M2S')
      if(any(IndexM < beg)) call MessageText('Error','S:IndConv:: Invalid input for "IndexM".')
      !FIXTHIS: no detection on upperbound.
      select case (MaxDim)
        case (1)
          IndexS = IndexM(1)
        case (2)
          IndexS = IndexM(1) + &
                 &(IndexM(2) - beg)*NforDim(1)
        case (3)
          IndexS = IndexM(1) + &
                 &(IndexM(2) - beg)*NforDim(1) + &
                 &(IndexM(3) - beg)*NforDim(2)*NforDim(1)
        case default
          call MessageText('Error','S:IndConv:: Invalid input for "MaxDim".')
      end select
    case ('S2M')
      if(indexS < beg) call MessageText('Error','S:IndConv:: Invalid input for "IndexS".')
      !FIXME: no detection on upperbound.
      select case (MaxDim)
        case (1)
          IndexM(1) = IndexS
        case (2)
          IndexM(2) = (IndexS - beg)/NforDim(1) + beg
          IndexM(1) = IndexS - (IndexM(2)- beg)*NforDim(1)
        case (3)
          IndexM(3) = (IndexS - beg)/(NforDim(1)*NforDim(2)) + beg
          IndexM(2) = (IndexS - (IndexM(3)- beg)*NforDim(1)*NforDim(2) - beg)/NforDim(1) + beg
          IndexM(1) = (IndexS - (IndexM(3)- beg)*NforDim(1)*NforDim(2) - (IndexM(2) - beg)*NforDim(1))
        case default
          call MessageText('Error','S:IndConv:: Invalid input for "MaxDim".')
      end select
    case default
      call MessageText('Error','S:IndConv:: Invalid input for "Dir".')
    end select
   
  end subroutine IndConv

  !---------------------------------------------------------------
  subroutine Stencil1D(n_point, order_derivative, x0, x, coef)
    implicit none
    integer, intent(in) :: n_point !< Total number of point involved in calculating derivative.
    integer, intent(in) :: order_derivative
    real(kind=dp), intent(in) :: x0 !< The point of interest for derivative.
    real(kind=dp), intent(in) :: x(n_point) 
    real(kind=dp), intent(out) :: coef(n_point)
    integer :: i, j
    integer :: factorial
    integer :: info, IPIV(n_point), work(n_point)
    real(kind=dp) :: M(n_point,n_point)

    if(mod(n_point,2) == 0) call MessageText('Error','S:Stencile1D:: "n_point" must be an odd number.')
    if(order_derivative <= 0) call MessageText('Error','S:Stencile1D:: "order_derivative" must be > 1.')

    do j = 1, n_point
      factorial = 1
      do i = 1, n_point
        factorial = factorial*(i - 1)
        if(i==1) factorial = 1
        M(i,j) = (x(j)-x0)**dble(i-1)/dble(factorial)
      end do
    end do

    call DGETRF(n_point,n_point,M,n_point,IPIV,info)
    if(info .ne. 0) call MessageText('Error','S:Stencile1D:: The execution of LAPACK subroutine DGETRF fails.')
    call DGETRI(n_point,M,n_point,IPIV,work,n_point,info)
    if(info .ne. 0) call MessageText('Error','S:Stencile1D:: The execution of LAPACK subroutine DGETRI fails.')
    coef(:) = M(:,order_derivative+1)

  end subroutine Stencil1D

  !----------------------------------------------------------------
  subroutine System(sys)
    implicit none
    type(system_t), intent(inout) :: sys

    call Parse('NumberOfState', sys%n_state)

  end subroutine System

  !---------------------------------------------------------------
  subroutine TimeFrequencyAnalysis(anls, td, sys, obsv, VelocityOrAccelerationForm, direction)
  implicit none
  type(analysis_t), intent(in) :: anls
  type(td_t), intent(in) :: td
  type(system_t), intent(in) :: sys
  type(observable_t), intent(in) :: obsv
  character, intent(in) :: VelocityOrAccelerationForm
  integer, intent(in) :: direction
  integer, parameter :: nt = 500
  real(kind=dp), parameter :: std_dev = 1.78d0*u_FemtoSecond/2.355d0 !2.355 is the factor for conversion between sigma and FWHM
  real(kind=dp) :: time(nt)
  real(kind=dp) :: dw
  real(kind=dp) :: tp(50), wp(50)
  real(kind=dp), allocatable :: current_tmp1(:)
  real(kind=dp), allocatable :: current_tmp2(:)
  real(kind=dp), allocatable :: hhg_f(:)
  real(kind=dp), allocatable :: window(:)
  real(kind=dp), allocatable :: TFA(:,:)
  complex(dp) :: csum
  integer :: i, t, t_window, t_eva
  integer :: WU_TFA

  open(newunit = WU_TFA, file = 'tfa.dat')

  ! Set up time array
  if(mod(td%Nt,nt) .ne. 0) call MessageText('Error','S:TimeFrequencyAnalysis:: Currently sys%td%Nt must be an integer multiple of the Nt in this subroutine.')
  do t = 1, nt
    time(t) = td%time(t*(td%Nt/nt)) !this is the constraint for above error. FIXME
  end do

  ! Set up frequency array
  dw = (anls%hhg_EU - anls%hhg_EL)/dble(anls%n_hhg - 1)
  allocate(hhg_f(anls%n_hhg))
  do i = 1, anls%n_hhg
    hhg_f(i) = dble(i-1)*dw + anls%hhg_EL
  end do

  allocate(current_tmp1(obsv%NtEvaluation))
  allocate(current_tmp2(obsv%NtEvaluation))
  allocate(window(td%Nt))
  allocate(TFA(anls%n_hhg, nt))
  window = 0.d0

  ! Applying the global window
  tp(1) = td%T_PulCen
  wp(1) = td%T_Pul
  call WindowGenerator(td%Nt-1, 5, td%time(1:td%Nt), tp, window(:), wp)
  do t_eva = 1, obsv%NtEvaluation
    current_tmp1(t_eva) = obsv%MechanicalMomentum(t_eva, direction)*window(obsv%EvGr2TmGr(t_eva)) !FIXME: current_tmp1 is not current but just mechanical momentum
  end do

  do t_window = 1, nt
    if(mod(t_window,10) == 0) print*, 'TFA:', t_window, 'among', nt
    tp(1) = time(t_window)
    wp(1) = std_dev
    call WindowGenerator(td%Nt-1, 3, td%time(1:td%Nt), tp, window(1:td%Nt), wp)
    do t_eva = 1, obsv%NtEvaluation
      current_tmp2(t_eva) = current_tmp1(t_eva)*window(obsv%EvGr2TmGr(t_eva))
    end do

    do i = 1, anls%n_hhg
      csum = dcmplx(0.d0,0.d0)
      do t_eva = 1, obsv%NtEvaluation
        if(abs(td%time(obsv%EvGr2TmGr(t_eva)) - time(t_window)) < 20.d0*std_dev) then
          if(t_eva == 1) then
            csum = csum + dcmplx(current_tmp2(t_eva)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva + 1)) - td%Time(obsv%EvGr2TmGr(t_eva    ))), 0.d0)*&
                         &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
          elseif(t_eva == obsv%NtEvaluation) then
            csum = csum + dcmplx(current_tmp2(t_eva)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva    )) - td%Time(obsv%EvGr2TmGr(t_eva - 1))), 0.d0)*&
                         &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
          else
            csum = csum + dcmplx(current_tmp2(t_eva)*0.5d0*(td%Time(obsv%EvGr2TmGr(t_eva + 1)) - td%Time(obsv%EvGr2TmGr(t_eva - 1))), 0.d0)*&
                         &cdexp(dcmplx(0.d0, -hhg_f(i)*td%Time(obsv%EvGr2TmGr(t_eva))))
          end if
          csum = csum + dcmplx(current_tmp2(t_eva),0.d0)*cdexp(dcmplx(0.d0,-hhg_f(i)*td%time(obsv%EvGr2TmGr(t_eva))))
        end if
      end do
      if(VelocityOrAccelerationForm == 'V') then
        TFA(i, t_window) = 2.d0*dlog10(cdabs(csum)) ! velocity form
      else
        TFA(i, t_window) = 2.d0*dlog10(cdabs(csum*dcmplx(0.d0, hhg_f(i)))) ! acceleration form
      end if
    end do
  end do

  ! Write the data
  write(WU_TFA,*) '#Time-frequency analysis'
  write(WU_TFA,*) '#Time(oc)  Energy(eV)  log10(Intensity)'
  do t_window = 1, nt
    do i = 1, anls%n_hhg
      write(WU_TFA,*) time(t_window)/td%period, hhg_f(i)/u_eV, TFA(i,t_window)
    end do
    write(WU_TFA,*) ''
  end do
  write(WU_TFA,*) ''

  end subroutine TimeFrequencyAnalysis
 
end module GlobalMDL
