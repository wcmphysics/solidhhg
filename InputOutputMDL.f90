module InputOutputMDL
use UnitAndConstantMDL
implicit none
Character(len = *), parameter :: DefaultNameInputFile = 'inp.inp'

private
public Parse, MessageText

interface Parse
  module procedure ParseLogical
  module procedure ParseInteger
  module procedure ParseDoubleReal
  module procedure ParseLogicalVector
  module procedure ParseIntegerVector
  module procedure ParseDoubleRealVector
  module procedure ParseLogicalMultipleVectors
  module procedure ParseIntegerMultipleVectors
  module procedure ParseDoubleRealMultipleVectors
end interface Parse

contains


  !----------------------------------------------------------------
  subroutine ParseLogical(NameVariable, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    Logical, intent(out) :: Output
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 1
    integer, parameter :: n = 1
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputL(n, m)

  end subroutine ParseLogical


  !----------------------------------------------------------------
  subroutine ParseInteger(NameVariable, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(out) :: Output
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 2
    integer, parameter :: n = 1
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputI(n, m)

  end subroutine ParseInteger


  !----------------------------------------------------------------
  subroutine ParseDoubleReal(NameVariable, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    real(kind = dp), intent(out) :: Output
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 3
    integer, parameter :: n = 1
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputR(n, m)

  end subroutine ParseDoubleReal


  !----------------------------------------------------------------
  subroutine ParseLogicalVector(NameVariable, n, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    Logical, intent(out) :: Output(n)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 1
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputL(:, m)

  end subroutine ParseLogicalVector


  !----------------------------------------------------------------
  subroutine ParseIntegerVector(NameVariable, n, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    integer, intent(out) :: Output(n)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 2
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputI(:, m)

  end subroutine ParseIntegerVector


  !----------------------------------------------------------------
  subroutine ParseDoubleRealVector(NameVariable, n, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    real(kind = dp), intent(out) :: Output(n)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile

    integer, parameter :: OutputTypeIndicator = 3
    integer, parameter :: m = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputR(:, m)

  end subroutine ParseDoubleRealVector


  !----------------------------------------------------------------
  subroutine ParseLogicalMultipleVectors(NameVariable, n, m, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    integer, intent(in) :: m
    Logical, intent(out) :: Output(n, m)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 1
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputL

  end subroutine ParseLogicalMultipleVectors


  !----------------------------------------------------------------
  subroutine ParseIntegerMultipleVectors(NameVariable, n, m, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    integer, intent(in) :: m
    integer, intent(out) :: Output(n, m)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
    
    integer, parameter :: OutputTypeIndicator = 2
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputI

  end subroutine ParseIntegerMultipleVectors


  !----------------------------------------------------------------
  subroutine ParseDoubleRealMultipleVectors(NameVariable, n, m, Output, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: n
    integer, intent(in) :: m
    real(kind = dp), intent(out) :: Output(n, m)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile

    integer, parameter :: OutputTypeIndicator = 3
    Logical :: OutputL(n, m)
    integer :: OutputI(n, m)
    real(kind=dp) :: OutputR(n, m)

    call ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    Output = OutputR

  end subroutine ParseDoubleRealMultipleVectors


  !----------------------------------------------------------------
  !>
  !! Keep in mind it drops anything after 256th character.
  subroutine ParseMultipleVectors(NameVariable, OutputTypeIndicator, n, m, OutputL, OutputI, OutputR, IsPrintInfo, NameFile)
    implicit none
    Character(len = *), intent(in) :: NameVariable
    integer, intent(in) :: OutputTypeIndicator
    integer, intent(in) :: n  ! length of a vector
    integer, intent(in) :: m  ! number of vectors
    Logical, intent(out) :: OutputL(n, m)
    integer, intent(out) :: OutputI(n, m)
    real(kind = dp), intent(out) :: OutputR(n, m)
    Logical, intent(in), optional :: IsPrintInfo
    Character(len = *), intent(in), optional :: NameFile
 
    Logical :: IsFound
    Logical :: IsBlockReadingComplete
    Logical :: IsPrintInfo_tmp
    Character(len = 256) :: buffer
    Character(len = 256) :: Expression
    Character(len = 256) :: NameFile_tmp
    integer :: IOstatus
    integer :: Line
    integer :: LineInsideBlock
    integer :: LineOfPresentBlock
    integer :: WU

    NameFile_tmp = trim(adjustl(DefaultNameInputFile))
    if (present(NameFile)) NameFile_tmp = trim(adjustl(NameFile))

    open(newunit = WU, file = trim(adjustl(NameFile_tmp)))

    IsPrintInfo_tmp = .false.
    if(present(IsPrintInfo)) IsPrintInfo_tmp = IsPrintInfo
    IsFound = .false.
    IOstatus = 0
    line = 0
    OutputI = 0
    OutputR = 0.d0

    do while (IOstatus == 0 .and. (.not. IsFound))

      line = line + 1
      read(WU, '(A)', iostat = IOstatus) buffer

      if (trim(adjustl(buffer(1:scan(buffer, '=') - 1))) == trim(adjustl(NameVariable))) then ! Reading a vector in a single line.
        IsFound = .true.
        Expression = buffer(scan(buffer, '=') + 1:)
        select case (OutputTypeIndicator)
          case (1) ! logical
            read(Expression, *, iostat = IOstatus) OutputL(:, 1)
            if (IsPrintInfo_tmp) then 
              write(*, *) 'Read from file: '       , trim(adjustl(NameFile_tmp)),&
                        & ' and read the variable ', trim(adjustl(NameVariable)),&
                        & ' at line '              , Line,&
                        & 'with value '            , OutputL(:, 1)
            end if
          case (2) ! integer
            read(Expression, *, iostat = IOstatus) OutputI(:, 1)
            if (IsPrintInfo_tmp) then 
              write(*, *) 'Read from file: '       , trim(adjustl(NameFile_tmp)),&
                        & ' and read the variable ', trim(adjustl(NameVariable)),&
                        & ' at line '              , Line,&
                        & 'with value '            , OutputI(:, 1)
            end if
          case (3) ! double real
            read(Expression, *, iostat = IOstatus) OutputR(:, 1)
            if (IsPrintInfo_tmp) then 
              write(*, *) 'Read from file: '       , trim(adjustl(NameFile_tmp)),&
                        & ' and read the variable ', trim(adjustl(NameVariable)),&
                        & ' at line '              , Line,&
                        & 'with value '            , OutputR(:, 1)
            end if
          case default
            call MessageText('Error', 'Invalid value for output indicator.', 'InputOutputMDL', 'ParseMultipleVectors')
        end select
        if (IOstatus .ne. 0) call MessageText('Error', 'The variable is found but its reading failed.', 'InputOutputMDL', 'ParseIntegerMultipleVectors')
      elseif (trim(adjustl(buffer)) == trim("%"//adjustl(NameVariable))) then ! Reading several vectors listed in several lines.
        IsFound = .true.
        IsBlockReadingComplete = .false.
        LineInsideBlock = 0
        LineOfPresentBlock = line
        do while (IOstatus == 0 .and. (.not. IsBlockReadingComplete) .and. (LineInsideBlock <= m))
          line = line + 1
          LineInsideBlock = LineInsideBlock + 1
          read(WU,'(A)', iostat = IOstatus) Expression
          if(trim(adjustl(Expression(1:1))) == "%") then
            IsBlockReadingComplete = .true.
          else
            select case (OutputTypeIndicator)
              case (1) ! logical
                read(Expression, *, iostat = IOstatus) OutputL(:, LineInsideBlock)
                if (IsPrintInfo_tmp) then 
                  write(*, *) 'Read from file: '             , trim(adjustl(NameFile_tmp)),&
                            & ' and read the block variable ', trim(adjustl(NameVariable)),&
                            & ' at line '                    , LineOfPresentBlock,&
                            & 'with value '                  , OutputL(:, LineInsideBlock)
                end if
              case (2) ! integer
                read(Expression, *, iostat = IOstatus) OutputI(:, LineInsideBlock)
                if (IsPrintInfo_tmp) then 
                  write(*, *) 'Read from file: '             , trim(adjustl(NameFile_tmp)),&
                            & ' and read the block variable ', trim(adjustl(NameVariable)),&
                            & ' at line '                    , LineOfPresentBlock,&
                            & 'with value '                  , OutputI(:, LineInsideBlock)
                end if
              case (3) ! double real
                read(Expression, *, iostat = IOstatus) OutputR(:, LineInsideBlock)
                if (IsPrintInfo_tmp) then 
                  write(*, *) 'Read from file: '             , trim(adjustl(NameFile_tmp)),&
                            & ' and read the block variable ', trim(adjustl(NameVariable)),&
                            & ' at line '                    , LineOfPresentBlock,&
                            & 'with value '                  , OutputR(:, LineInsideBlock)
                end if
              case default
                call MessageText('Error', 'Invalid value for output indicator.', 'InputOutputMDL', 'ParseMultipleVectors')
            end select
          end if
          if (IOstatus .ne. 0) call MessageText('Error', 'The block variable is found but its reading failed.', 'InputOutputMDL', 'ParseIntegerMultipleVectors')
        end do
      end if
    end do

    close(WU)
    if(.not. IsFound) call MessageText('Error', 'The designated variable is not found.', 'InputOutputMDL', 'ParseMultipleVectors')

  end subroutine ParseMultipleVectors


  !----------------------------------------------------------------
  subroutine MessageText(MessageType, TextContent, NameMDL, NameSub, Indent, IsStopped)
    implicit none
    character(len = *), intent(in) :: MessageType
    character(len = *), intent(in) :: TextContent
    character(len = *), intent(in), optional :: NameMDL
    character(len = *), intent(in), optional :: NameSub
    logical, intent(in), optional :: Indent
    logical, intent(in), optional :: IsStopped

    logical :: IsStopped_tmp
    character(len = 3) :: adv

    adv = "yes"
    if (present(Indent) .and. Indent == .false.) adv = "no"

    IsStopped_tmp = .true.
    if (present(IsStopped)) IsStopped_tmp = IsStopped

    select case (trim(MessageType))
      case ("Fatal", "fatal")
        if (present(NameMDL) .and. present(NameSub)) then 
          write(*, '("!!!! Fatal:", 1X, A, ":", 1X, A, ":", 1X, A)') NameMDL, NameSub, TextContent
        else
          write(*, '("!!!! Fatal:", 1X, A)') TextContent
        end if
        if (IsStopped_tmp) stop

      case ("Error", "error")
        if (present(NameMDL) .and. present(NameSub)) then
          write(*, '("!!!! Error:", 1X, A, ":", 1X, A, ":", 1X, A)') NameMDL, NameSub, TextContent
        else
          write(*, '("!!!! Error:", 1X, A)') TextContent
        end if
        if (IsStopped_tmp) stop

      case ("Warning", "warning")
        if (present(NameMDL) .and. present(NameSub)) then 
          write(*, '("#### Warning:", 1X, A, ":", 1X, A, ":", 1X, A)', advance = trim(adv)) NameMDL, NameSub, TextContent
        else
          write(*, '("#### Warning:", 1X, A)', advance = trim(adv)) TextContent
        end if

      case ("Info", "info")
        if (present(NameMDL) .and. present(NameSub)) then 
          write(*, '("---- Info:", 1X, A, ":", 1X, A, ":", 1X, A)', advance = trim(adv)) NameMDL, NameSub, TextContent
        else
          write(*, '(A)', advance = trim(adv)) TextContent
        end if

      case default
        write(*, '("!!!! Error: InputOutputMDL: MessageText:", 1X, A)') 'Invalid message type.'
        stop
    end select 

  end subroutine MessageText

end module InputOutputMDL
