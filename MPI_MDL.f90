module MPI_MDL
use UnitAndConstantMDL
use InputOutputMDL
use mpi

private

public :: MPI_Index
public :: tester

contains

  !---------------------------------------------------------------
  !> Temporary subroutine for testing.
  subroutine tester()
    implicit none
    integer :: mpi_err, nworker, worker
    real(kind=dp) :: time_wall
    real(kind=dp) :: a = 1.d0

    integer :: id, tag, source, n, ist, ifi
    logical IsThisWorkerWait
   
    call MPI_Init(mpi_err)
    call MPI_Comm_Size( MPI_COMM_WORLD, nworker, mpi_err)
    call MPI_Comm_Rank( MPI_COMM_WORLD, worker, mpi_err)
    if(worker == 0 ) then
      print*, '===================='
      time_wall = MPI_Wtime()
      print*, 'this is the host.'
      print*, 'worker, nworker', worker, nworker
      a =2.d0
      print*, 'tmp in worker 0 after manipulation', a
      print*, '==================='
      id = 63
      tag = 0
      call MPI_Send(a, 1, MPI_DOUBLE, id, tag, MPI_COMM_WORLD, mpi_err)
    end if 
    call MPI_Barrier(MPI_COMM_WORLD, mpi_err)
   
    if(worker == 63) then
      source = 0
      tag = 0
      call MPI_Recv(a, 1, MPI_DOUBLE, source, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_err)
    end if
    n = 33
    call MPI_Index(n, IsThisWorkerWait, ist, ifi, 'Conv1', 'ReportAndPrint')
    print*, 'Inside: worker', worker, a
   
    call MPI_Finalize(mpi_err)
   
  end subroutine tester

  !---------------------------------------------------------------
  !> Determine the starting and ending index for MPI processes.
  subroutine MPI_Index(n, IsThisWorkerWait, i_first, i_last, Conv0or1, Report)
    implicit none
    integer, intent(in) :: n ! total number of tasks
    logical, intent(out) :: IsThisWorkerWait
    integer, intent(out) :: i_first
    integer, intent(out) :: i_last
    character(len=*), optional, intent(in) :: Conv0or1
    character(len=*), optional, intent(in) :: Report

    integer :: nworker
    integer :: worker
    integer :: chunk
    integer :: residual
    integer :: mpi_err
    character(len=5) :: ConvFlag
    character(len=14) :: Report_tmp

    IsThisWorkerWait = .false.
    ConvFlag = 'Con1'
    if(present(Conv0or1)) ConvFlag = trim(adjustl(Conv0or1))
    if(.not.(ConvFlag == 'Conv0' .or. ConvFlag == 'Conv1')) call MessageText('Error','S:MPI_Index:: Invalid character input "Conv0or1". It could only be "Conv0" or "Conv1". ')

    call MPI_Comm_Size( MPI_COMM_WORLD, nworker, mpi_err)
    call MPI_Comm_Rank( MPI_COMM_WORLD, worker, mpi_err)
    chunk = n/nworker
    residual = mod(n, nworker)

    if(worker+1 <= residual ) then
      i_first = worker*(chunk + 1) + 1
      i_last = i_first + chunk
    else
      i_first = worker*(chunk) + residual + 1
      i_last = i_first + chunk - 1
      if(chunk == 0) IsThisWorkerWait = .true.
    end if

    if(ConvFlag == 'Conv0') then
      i_first = i_first - 1 
      i_last = i_last - 1 
    end if
    
    Report_tmp = 'NoReport'
    if(present(Report)) Report_tmp =trim(adjustl(Report))
    select case(trim(adjustl(Report_tmp)))
      case ('NoReport')
        ! Do nothing
      case ('Report')
        ! FIXME: this should write data to output file monitoring the running program.
      case('ReportAndPrint')
        if(worker == 0) write(*, '(A,1X,A,1X,A,1X,A,1X,A,1X,A,1X,A)') '# of worker', '     tasks', 'worker', '     chunk', '     start', '    finish', 'idle?'
        call MPI_Barrier(MPI_COMM_WORLD, mpi_err)
        write(*, '(I11,1X,I10,1X,I6,1X,I10,1X,I10,1X,I10,1X,L5)') nworker, n, worker, i_last - i_first + 1, i_first, i_last, IsThisWorkerWait
      case default
      if(.not.(Report == 'NoReport' .or. Report == 'Report' .or. Report == 'ReportAndPrint')) then
        if(worker == 0) call MessageText('Error','S:MPI_Index:: Invalid input "Report". It could only be "NoReport", "Report", or "ReportAndPrint". ')
      end if
    endselect

  end subroutine MPI_Index

end module MPI_MDL
