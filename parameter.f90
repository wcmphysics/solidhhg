module ParameterMDL
use UnitAndConstantMDL
implicit none
!integer, parameter :: InputMaxDim = 2 !< Dimensionality of the system(must <=3).
!integer, parameter :: InputBoundaryCondition(3) = (/1, 1, 1/) !< 0 and 1 for infinite well and periodic boundary condition, respectively.
!integer, parameter :: InputNr(3) = (/15, 15, 10/) !< real space points must >=3 (due to 2nd derivative)
!integer, parameter :: InputNk(3) = (/15, 15, 5/) !< for non-periodic dimension Nk should be one; for periodic dimension Nk must >=3 (due to first derivative in calculating slope of a band)
!integer, parameter :: InputNt = 100000
!integer, parameter :: InputNhhg = 2000
!integer, parameter :: InputExternalPotentialChoice = 2 !< 1 for Mathieu-type , and 2 for inseparable Mathieu-type crystal potential.
!integer, parameter :: InputExternalPotentialChoice = 1 !< 1 for Mathieu-type , and 2 for inseparable Mathieu-type crystal potential.
!integer, parameter :: InputGroundSolveMethodChoice = 1 !< 1 for forming a matrix and solved by LAPACK.
!integer, parameter :: InputTimePropagationMethodChoice = 0 !< 0 for direct application, and 1 for matrix vector multiplication(InputGroundSolveMethodChoice must be 1 in this case).
!integer, parameter :: InputGround_NSolve = 6
!integer, parameter :: InputHouston_NSolve = 6
!integer, parameter :: InputNStencil = 5
!integer, parameter :: InputNState = 3 !< The number of electron in a cell. It must be >=1. No spin in our system.
!integer, parameter :: InputNState = 2 !< The number of electron in a cell. It must be >=1. No spin in our system.
!integer, parameter :: InputExpOrder = 4
!!integer, parameter :: InputNumberOfPointForTimeObservableEvaluation = 100000 !< This must be >= 3, and smaller than InputNt. Evaluation is always performed at first and last time grids. 


!real(kind=dp), parameter :: InputRShift(3) = (/0.d0, 0.d0, 0.d0/) 
!real(kind=dp), parameter :: InputBoxL(3) = 3.430473684210526d0
!real(kind=dp), parameter :: InputBoxL(3) = (/8.d0, 1.d0, 8.d0/) !< Height of Mathieu-type potential.
!real(kind=dp), parameter :: InputV0(3) = 0.9715263151871530d0
!real(kind=dp), parameter :: InputV0(3) = (/0.37d0, 0.d0, 0.37d0/)
!real(kind=dp), parameter :: InputTotalPropagationTime = 100.d0 * u_FemtoSecond
!real(kind=dp), parameter :: InputTotalPulseTime = 96.0664594176350874d0* u_FemtoSecond
!real(kind=dp), parameter :: InputPulseCenterTime = InputTotalPulseTime / 2.d0
!real(kind=dp), parameter :: InputWavelength = 3200.d0  ! wavelength in nm. 3200 for 1D
!real(kind=dp), parameter :: InputE0(3) = (/0.165d0, 0.d0, 0.d0/) !< E-field amplitude in Volt/Angstrom. 0.165
!real(kind=dp), parameter :: InputHHGLowerEnergy = 0.d0 *u_eV
!real(kind=dp), parameter :: InputHHGUpperEnergy = 120.d0 *u_eV

! V0                     L
! 0.9715263151871530d0   3.430473684210526d0   0.4197988979811514E+01   0.8303258201124926E-01   0.4770889425218647E-01 for r15 k15
! 0.7666206896551724d0   3.978620689655172d0   0.4182284690182540E+01   0.8308729361554615E-01   0.2196179359684927E-01 for r15 k31
! 0.7454545454545455d0   4.051515151515152d0   0.4186196598003975E+01   0.8303562055844618E-01   0.8151864949053311E-01 for r15 k61
!ExternalPotentialFourierCompX = 0.515034663280809 0.740147214833825 -0.354344733487580 7.149413877372168E-002 -9.611797013591827E-003 -1.656679688228913E-004
!ExternalPotentialFourierCompY = 0.967139361295389 0.509255423570436 -0.154467280995563 -0.180550596613410      1.680797107385967E-002  3.155730388309741E-003
public

end module ParameterMDL
